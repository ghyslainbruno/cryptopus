#!/bin/bash

if [ "$1" = "prod" ]
	then 

	if [ -z "$2" ]
		then
		echo 'No version number - add a version number as 2nd parameter for production build -> ex: ./builder.sh prod 0.0.1'
	else 

		versionNumber="$2"

		# Getting all jsFiles to minify and concatenate in a variable (DO IT FIRSTLY !) | When jsFiles are commented
		jsFiles=`cat app/index.html | awk '/<!--START-->/,/<!--END-->/' | grep '<script' | sed 's/<\!--//' | sed 's/-->$//' | sed -e 's/.*\(js\/.*.js\).*$/\1/' | sed -e 's/\(^.*$\)/app\/\1/'`

		# Minify all JS files in the index.mmin.js
		uglifyjs $jsFiles --compress --mangle > app/index.min.js

		# Making a copy of the index.html inside the tmp.html before doing this
		cat app/index.html > app/before.html

		# Comment develop scripts & Uncomment index.min.js -> reference the index.min.js file inside the index.html
		sed -e 's/\(<script src=\"js.*pt>\)$/<!--\1-->/' app/index.html | sed -e 's/^.*\(<script\ src=\"index.min.js.*\"><\/script>\).*$/\ \ \ \ <script\ src=\"index.min.js?v'"${versionNumber}"'\"><\/script>/' | sed -e 's/\(css\/perso\.css\).*\"/\1?v'"${versionNumber}"'\"/' > app/tmp.html

		cat app/tmp.html > app/index.html

		echo 'Production build done'
	fi

elif [ "$1" = "dev" ]
	then

	# Make a copy of before file in app/tmp.html
	cat app/index.html > app/before.html
	# Uncomment develop scripts insert & Comment index.min.js insertion
	sed -e 's/<!--\(<script src=\"js.*\)-->$/\1/' app/index.html | sed -e 's/<script\ src=\"index.min.js.*\"><\/script>/<!--<script\ src=\"index.min.js\"><\/script>-->/' > app/tmp.html

	cat app/tmp.html > app/index.html

	echo 'Development build done'

fi


# Getting all jsFiles to minify and concatenate in a variable (DO IT FIRSTLY !) | When jsFiles are commented
#jsFiles=`cat app/index.html | awk '/<!--START-->/,/<!--END-->/' | grep '<script' | sed 's/<\!--//' | sed 's/-->$//'`

# Uncomment develop scripts insert
#sed -e 's/<!--\(<script src=\"js.*\)-->$/\1/' app/index.html

# Comment index.min.js insertion
#sed -e 's/<script\ src=\"index.min.js\"><\/script>/<!--<script\ src=\"index.min.js\"><\/script>-->/' app/index.html


# Comment develop scripts insert
#sed -e 's/\(<script src=\"js.*\)$/<!--\1-->/' app/index.html

# Uncomment index.min.js insertion
#sed -e 's/<!--<script\ src=\"index.min.js\"><\/script>-->/<script\ src=\"index.min.js\"><\/script>/' app/index.html

