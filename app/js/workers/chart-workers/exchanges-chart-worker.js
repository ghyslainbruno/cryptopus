onmessage = function(event) {

    var exchange = event.data.exchange;
    var timeData = event.data.historic;

    var data = {};
    var dataChart = [];
    var backgrounds = [];
    var labels = [];

    for (timeStampHolding in timeData) {

        var timeStampData = timeData[timeStampHolding];

        if (timeStampData[exchange] !== undefined || Object.keys(timeData).filter(data => data[exchange]).length !== 0) {
            var point = {};
            point.x = timeData[timeStampHolding].date;
            point.y = timeStampData[exchange].total_in_euros.toFixed(2);

            dataChart.push(point);

            labels.push(new Date(point.x).toLocaleDateString('en-EN', {'day': '2-digit', 'month': 'short'}));

            data[exchange] = {
                data: dataChart,
                labels: labels
            };
        }
    }
    data.exchange = exchange;

    postMessage(data);
};