onmessage = function(event) {

    // var exchange = event.data.exchange;
    const timeData = event.data;

    let data = {};
    const dataChart = [];
    const backgrounds = [];
    const labels = [];

    // Each time stamp
    for (timeStampHolding in timeData) {

        let timeStamp = timeData[timeStampHolding];
        const point = {};
        let totalInEurForThisTimeStamp = 0;

        // Each exchange/date for aa timeStamp
        for (exchange in timeStamp) {

            if (exchange === 'date') {
                point.x = timeStamp[exchange];
            } else {
                totalInEurForThisTimeStamp += timeStamp[exchange].total_in_euros;
            }

        }

        point.y = totalInEurForThisTimeStamp;
        dataChart.push(point);
        labels.push(new Date(point.x).toLocaleDateString('en-EN', {'day': '2-digit', 'month': 'short'}));
        data = {
            data: dataChart,
            labels: labels
        };

    }
    postMessage(data);

};