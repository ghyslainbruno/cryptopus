// Trying to make chart in global (TODO: change this) to be able to destroy its instance
let exchangesChart = null;
let currenciesChart = null;

Chart.defaults.global.legend.labels.usePointStyle = true;

function calculateHoldings() {

    try {
        $("#calculateLoader").show();
        $("#calculateBtn").addClass('disabled');

        firebase.auth().currentUser.getIdToken().then(function (token) {
            $.ajax({
                type: "GET",
                url:"/holdings",
                headers: {token : token},
                data: {all: true},
                success: function(data) {
                    if (data === "error") { Materialize.toast("Something went wrong", 1000)}

                    if (Object.keys(data).length === 0) {
                        $("#calculateLoader").hide();
                        $("#calculateBtn").removeClass('disabled');

                        goToExchanges();
                        Materialize.toast("First, enter some exchanges API keys (only those in read mode !)", 5000);
                    } else {

                        buildDetailsView(data, function(error) {


                            $(".collapsible").collapsible({
                                onOpen: function(el) {
                                    if (el.prevObject.length === 1) {
                                        el.prevObject[0].style.backgroundColor = '#f5f5f5';
                                    }
                                },
                                onClose: function(el) {
                                    if (el.prevObject.length === 1) {
                                        el.prevObject[0].style.backgroundColor = 'white';
                                    }
                                }
                            });

                            // Getting the 6 lasts historic data (to show the 24h delta in user's holdings)
                            firebase.database().ref(firebase.auth().currentUser.uid + '/historic').orderByChild('date').limitToLast(6).on('value', function(snapshot) {

                                let deltaTotal = 0;
                                let todayTotal = 0;
                                let yesterdayTotal = 0;
                                let percentages = 0;

                                if (snapshot.val() !== null) {
                                    // Getting the minimum date of all the data
                                    const dateMin = Object.values(snapshot.val()).reduce((min, p) => p.date < min ? p.date : min, Object.values(snapshot.val())[0].date);

                                    // Getting the historic holdings of the last day
                                    const yesterdayHistoricData = Object.values(snapshot.val()).find((obj) => obj.date = dateMin);
                                    delete yesterdayHistoricData['date'];

                                    todayTotal = Object.keys(data).reduce(function(sum, key) {

                                        if (data[key].total_in_euros !== null) {
                                            return sum + parseFloat( data[key].total_in_euros )
                                        } else {
                                            return sum + parseFloat(0);
                                        }

                                    }, 0);
                                    yesterdayTotal = Object.keys(yesterdayHistoricData).reduce(function(sum, key) {
                                        if (yesterdayHistoricData[key].total_in_euros !== null) {
                                            return sum + parseFloat(yesterdayHistoricData[key].total_in_euros);
                                        } else {
                                            return sum + parseFloat(0);
                                        }
                                    }, 0);
                                    deltaTotal = todayTotal - yesterdayTotal;
                                    percentages = (deltaTotal/((todayTotal + yesterdayTotal)/2))*100;
                                }


                                $("#holdingsDelta").text(deltaTotal.toFixed(2) + '€');
                                $("#holdingsDeltaPercentage").text(percentages.toFixed(2) + '%');

                                $("#calculateLoader").hide();
                                $("#calculateBtn").removeClass('disabled');

                                // Display the sum of all holdings for enabled exchanges
                                $("#holdings").text(Object.keys(data).reduce(function(sum, key) {
                                    if (data[key].total_in_euros !== null) {
                                        return sum + parseFloat(data[key].total_in_euros);
                                    } else {
                                        return sum + parseFloat(0);
                                    }
                                }, 0).toFixed(2) + "€");


                                if (deltaTotal > 0) {
                                    $("#arrowsDelta").html('<i class="material-icons">arrow_drop_up</i>');
                                    $("#arrowsDeltaPercentage").html('<i class="material-icons">arrow_drop_up</i>');

                                    document.getElementById("arrowsDelta").style.color = 'green';
                                    document.getElementById("holdingsDelta").style.color = 'green';

                                    document.getElementById("arrowsDeltaPercentage").style.color = 'green';
                                    document.getElementById("holdingsDeltaPercentage").style.color = 'green';
                                } else {
                                    $("#arrowsDelta").html('<i class="material-icons">arrow_drop_down</i>');
                                    $("#arrowsDeltaPercentage").html('<i class="material-icons">arrow_drop_down</i>');

                                    document.getElementById("arrowsDelta").style.color = 'red';
                                    document.getElementById("holdingsDelta").style.color = 'red';

                                    document.getElementById("arrowsDeltaPercentage").style.color = 'red';
                                    document.getElementById("holdingsDeltaPercentage").style.color = 'red';
                                }

                                if (!error) {
                                    makeExchangesRepartitionChart(data);
                                    makeCurrenciesRepartitionChart(data);
                                }

                            });
                        });

                    }
                },
                error: function(error) {
                    Materialize.toast(error.responseText, 1000);
                    $("#calculateLoader").hide();
                    $("#calculateBtn").removeClass('disabled');

                    if (error.status === 500) {
                        goToExchanges();
                        Materialize.toast("First, enter some exchanges API keys (only those in read mode !)", 5000);
                    }
                }
            })
        });
    } catch (error) {
        Materialize.toast(error, 2000);
    }

}

function makeExchangesRepartitionChart(data) {
    // Exchanges repartition chart
    const ctx = document.getElementById('homeChart').getContext('2d');
    // ctx.height = 10;

    let dataChart = [];
    let backgrounds = [];
    let labels = [];

    for (exchangeHolding in data) {

        r = Math.floor(Math.random() * 200);
        g = Math.floor(Math.random() * 200);
        b = Math.floor(Math.random() * 200);
        v = Math.floor(Math.random() * 500);
        c = 'rgb(' + r + ', ' + g + ', ' + b + ')';
        h = 'rgb(' + (r+20) + ', ' + (g+20) + ', ' + (b+20) + ')';

        dataChart.push(data[exchangeHolding].total_in_euros.toFixed(2));
        labels.push(exchangeHolding);
        backgrounds.push(c);
    }

    let options = {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            display: true,
            position: 'bottom',
        },
        tooltips: {
            callbacks: {
                label: function(tooltipsItem, data) {
                    return data.labels[tooltipsItem.index] +': ' + data.datasets[tooltipsItem.datasetIndex].data[tooltipsItem.index] + ' €';
                }
            }
        }
    } ;

    if (exchangesChart !== null) {
        exchangesChart.destroy();
    }

    exchangesChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            datasets: [{
                label: 'Exchanges repartition',
                data: dataChart,
                backgroundColor: backgrounds
            }],
            labels: labels
        },
        options: options
    });
}

function makeCurrenciesRepartitionChart(data) {

    const ctx = document.getElementById('homeCurrenciesChart').getContext('2d');

    let dataChart = [];
    let backgrounds = [];
    let labels = [];

    const currenciesRepartition = {};

    for (exchangeHolding in data) {

        const exchange = data[exchangeHolding];

        for (currency in exchange) {
            if (currency === 'EUR') {
                if (currenciesRepartition[currency] === undefined) {
                    currenciesRepartition[currency] = exchange[currency].balance;
                } else {
                    currenciesRepartition[currency] += exchange[currency].balance;
                }
            } else {
                if (currency !== 'total_in_euros') {

                    if (currenciesRepartition[currency] === undefined) {
                        currenciesRepartition[currency] = exchange[currency].balance_in_euros;
                    } else {
                        currenciesRepartition[currency] += exchange[currency].balance_in_euros;
                    }
                }
            }
        }
    }

    // To use the data when clicking on "hide < 1€ balances"
    // sessionStorage.setItem('currenciesRepartition', currenciesRepartition);

    for (currency in currenciesRepartition) {
        r = Math.floor(Math.random() * 200);
        g = Math.floor(Math.random() * 200);
        b = Math.floor(Math.random() * 200);
        v = Math.floor(Math.random() * 500);
        c = 'rgb(' + r + ', ' + g + ', ' + b + ')';
        h = 'rgb(' + (r+20) + ', ' + (g+20) + ', ' + (b+20) + ')';

        if (currenciesRepartition[currency] > 1) {
            dataChart.push(currenciesRepartition[currency].toFixed(2));
            labels.push(currency);
            backgrounds.push(c);
        }


    }

    let options = {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            display: true,
            position: 'bottom',
        },
        tooltips: {
            callbacks: {
                label: function(tooltipsItem, data) {
                    return data.labels[tooltipsItem.index] +': ' + data.datasets[tooltipsItem.datasetIndex].data[tooltipsItem.index] + ' €';
                }
            }
        }
    } ;

    if (currenciesChart !== null) {
        currenciesChart.destroy();
    }

    currenciesChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            datasets: [{
                label: 'Currencies repartition',
                data: dataChart,
                backgroundColor: backgrounds
            }],
            labels: labels
        },
        options: options
    });

}