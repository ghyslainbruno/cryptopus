function showingExchangesDivs() {
    fillExchangesApi();
    $("#applicationLoader").hide();
    $("#userCard").hide();
    $("#home").hide();
    $("#historicCharts").hide();
    $("#nex-features").hide();

    $("#exchangesSettings").show();
}

function showingHistoricDivs() {
    loadHistoricGraphs();
    $("#applicationLoader").hide();
    $("#exchangesSettings").hide();
    $("#home").hide();
    $("#userCard").hide();
    $("#nex-features").hide();

    $("#exchangesHistoricGraphLoader").show();
    $("#totalHistoricGraphLoader").show();
    $("#historicCharts").show();

}

function showingHomePage() {
    $("#applicationLoader").hide();
    $("#exchangesSettings").hide();
    $("#userCard").hide();
    $("#historicCharts").hide();
    $("#nex-features").hide();

    $("#home").show();
}

function showingNextFeaturesPage() {

    attachVotesListeners();

    $("#applicationLoader").hide();
    $("#exchangesSettings").hide();
    $("#userCard").hide();
    $("#historicCharts").hide();
    $("#home").hide();

    $("#nex-features").show();
}

function userAllowed(user) {

    // Fill sidenav
    $("#userImage").attr("src",user.photoURL);
    $("#userName").text(user.displayName);
    $("#userMail").text(user.email);
    // $("#versionNumber").text('v' + sessionStorage.getItem('versionNumber'));

    attachVersionNumberListener();

    // buildDetailsView();

    // Should be done when clicking "exchanges" in sidenav instead
    // fillExchangesApi(user);

    $("#applicationDiv").show();
    $("#userCard").hide();

    // var db = firebase.database();
}

function userNotAllowed() {

    $("#applicationDiv").hide();
    // $("#googleAuthBtn").show();
    $("#userCard").show();

    if (sessionStorage.getItem('firstTimeLaunch') || sessionStorage.getItem('firstTimeLaunch') === null) {

        initializeTuto();

    } else {
        $("#signInDivs").show();
    }
}

function goToExchanges() {
    // $("#applicationLoader").show();
    router.navigate('exchanges');
    // $('.button-collapse').sideNav('hide');
}

function goToHome() {
    // $("#applicationLoader").show();
    router.navigate('home');
    // $('.button-collapse').sideNav('hide');
}

function goToHistoric() {
    // $("#applicationLoader").show();
    router.navigate('historic');
    // $('.button-collapse').sideNav('hide');
}

function notYetImplemented() {
    Materialize.toast('Not yet implemented', 1000);
}

function hideSideNav() {
    $('.button-collapse').sideNav('hide');
}