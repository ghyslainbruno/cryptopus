function initializeTuto() {
    $("#tuto").show();

    document.getElementById("tuto").style.display = 'flex';

    if (document.getElementById("slick") !== null) {
        if (!document.getElementById("slick").className.toString().includes('slick-initialized')) {
            $('.your-class').slick({
                appendArrows: $("#testBtn"),
                arrows: true,
                dots: true,
                infinite: false,
                prevArrow: '<a class="btn-floating waves-effect darken-text-2" style="background-color: #0d47a1" id="prevTutoBtn"><i class="material-icons left">chevron_left</i></a>',
                nextArrow: '<a class="btn-floating waves-effect darken-text-2" style="background-color: #0d47a1" id="prevTutoBtn"><i class="material-icons left">chevron_right</i></a>'
            });
        }
    }


    // const carousel = $('.carousel.carousel-slider').carousel({
    //     fullWidth: true,
    //     noWrap: true,
    //     onCycleTo: function(data) {
    //         if ($("#lastTutoPage").is(".active")) {
    //             $("#nextTutoBtn").addClass('disabled');
    //             $("#prevTutoBtn").removeClass('disabled');
    //         } else if ($("#firstTutoPage").is(".active")) {
    //             $("#nextTutoBtn").removeClass('disabled');
    //             $("#prevTutoBtn").addClass('disabled');
    //         } else {
    //             $("#nextTutoBtn").removeClass('disabled');
    //             $("#prevTutoBtn").removeClass('disabled');
    //         }
    //     }
    // });
    //
    // // Disable all swipping on carousel
    // if (typeof window.ontouchstart !== 'undefined') {
    //     carousel.off('touchstart.carousel');
    // }
    // // carousel.off('mousedown.carousel');
}

function showSignInDivs() {
    $("#tuto").hide();
    $("#signInDivs").show();
    document.getElementById("userCard").style.marginTop = '3vh';
}