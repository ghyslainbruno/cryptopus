
function afterAllHtmlViewsAreLoaded() {

    // Code which runs in here is done after all html template files are loaded (if everything is allright...)

    $(document).ready(function() {
        const root = null;
        const useHash = true; // Defaults to: false
        const hash = '#!'; // Defaults to: '#'
        const router = new Navigo(root, useHash, hash);

        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                userAllowed(user);
                // sessionStorage.clear();

                router
                    .on('exchanges', function () {
                        showingExchangesDivs();
                        router.updatePageLinks();
                    })
                    .on('historic', function() {
                        // showingHomePage();
                        showingHistoricDivs();
                        router.updatePageLinks();
                    })
                    .on('home', function() {
                        showingHomePage();
                        router.updatePageLinks();
                    })
                    .on('features', function() {
                        showingNextFeaturesPage();
                        router.updatePageLinks();
                    })
                    .on(function() {
                        showingHomePage();
                        // to trigger everytime
                        checkForRedirectResult();
                    })
                    .resolve();

                // Initializing sideNav component
                $(".button-collapse").sideNav();

                $('.modal').modal({
                    dismissible: false
                });


                window.router = router;

            } else {
                userNotAllowed();

                router
                    .on(function() {
                        userNotAllowed();
                        router.updatePageLinks();
                        //TODO: make an unauthorized page to tell user to login
                        checkForRedirectResult();
                    })
                    .resolve();

                // Initializing sideNav component
                $(".button-collapse").sideNav();

                // Initializing modal view to display only when user already exists with the same email in database
                $('.modal').modal({
                    dismissible: false
                });
            }
        });
    });

    // firebase.database().ref('version').once('value')
    //     .then(function(snapshot) {
    //         $("#versionNumber").text('v' + snapshot.val());
    //         console.log(snapshot);
    //     })
    //     .catch(function(error) {
    //         console.log(error);
    //     });

    // Biding authentication buttons click events
    $("#googleAuthBtn").click(function() {
        googleAuthenticate(function(error, user) {
            if (!error) {
                // Materialize.toast(user, 3000);
                console.log(user);
            } else {
                // Materialize.toast(error.message, 3000);
                console.log(error.message);
            }
        })
    });

}