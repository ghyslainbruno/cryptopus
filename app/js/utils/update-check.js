function attachVersionNumberListener() {
    firebase.database().ref('version').on('value',function(snapshot){

        let localVersionNumber = localStorage.getItem('versionNumber');

        if (localVersionNumber === null) {
            localStorage.setItem('versionNumber', snapshot.val());
            localVersionNumber = localStorage.getItem('versionNumber');
        }

        if (localVersionNumber !== snapshot.val()) {

            const $toastContent = $('<span>New version available</span>').add($('<button onclick="updateVersionNumber(\'' + snapshot.val() + '\')" class="btn-flat toast-action">Reload</button>'));
            Materialize.toast($toastContent);
        } else {
            $("#versionNumber").text('v' + localStorage.getItem('versionNumber'));
        }
    });
}

function updateVersionNumber(number) {

    // Get toast DOM Element, get instance, then call remove function
    const toastElement = $('.toast').first()[0];
    const toastInstance = toastElement.M_Toast;
    toastInstance.remove();

    localStorage.setItem('versionNumber', number);
    window.location.reload(true);
}

