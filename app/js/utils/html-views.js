$(function(){
    const includes = $('[data-include]');

    // Dirty and tricky way to wait until every html templates are loaded before doing something else

    let i = 0;
    jQuery.each(includes, function(){
        const file = '/views/' + $(this).data('include').replace('-','/') + '.html';
        $(this).load(file, function() {
            i++;

            if (i === includes.length) {
                afterAllHtmlViewsAreLoaded();
            }
        });
    });
});


