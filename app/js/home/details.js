// Build Details View

let ordersHistoryChart = null;

function buildDetailsView(data, callback) {

    try {
        // Deleting actual details
        const detailsList = document.getElementById("detailsList");
        if (detailsList !== null) {
            while (detailsList.firstChild) {
                detailsList.removeChild(detailsList.firstChild);
            }
        }


        // Getting currencies logos
        $.ajax({
            type: "GET",
            url:"https://min-api.cryptocompare.com/data/all/coinlist",
            data: data,
            success: function(cryptocompareData) {

                const baseLogoUrl = cryptocompareData.BaseImageUrl;

                firebase.database().ref(firebase.auth().currentUser.uid + '/historic').orderByChild('date').limitToLast(6).on('value', function(snapshot) {
                    Object.keys(data).forEach(function(enabledExchange) {
                        const ul = document.getElementById("detailsList");

                        const li = document.createElement("li");

                        const title = document.createElement("div");
                        title.className = 'collapsible-header';

                        const body = document.createElement("div");
                        body.className = 'collapsible-body';
                        body.style.padding = 0;

                        // li.className = 'collection-item';

                        if (data[enabledExchange].total_in_euros !== null) {
                            title.appendChild(document.createTextNode(enabledExchange + ": " + data[enabledExchange].total_in_euros.toFixed(2) + " €"));
                        } else {

                            const errorIcon = document.createElement('i');
                            errorIcon.className = 'material-icons';
                            errorIcon.style.color = 'red';
                            errorIcon.appendChild(document.createTextNode('error'));

                            const errorIconWrapper = document.createElement('div');
                            errorIconWrapper.className = 'valign-wrapper';
                            errorIconWrapper.style.width = '50%';
                            errorIconWrapper.appendChild(errorIcon);

                            const exchangeName = document.createElement('p');
                            exchangeName.style.margin = '0';
                            exchangeName.innerHTML = enabledExchange;
                            exchangeName.style.width = '50%';

                            const titleWithErrorIcon = document.createElement('div');
                            titleWithErrorIcon.style.width = '100%';
                            titleWithErrorIcon.appendChild(exchangeName);
                            titleWithErrorIcon.style.display = 'flex';
                            titleWithErrorIcon.appendChild(errorIconWrapper);

                            title.appendChild(titleWithErrorIcon);
                        }

                        const currencyList = document.createElement('ul');
                        currencyList.className = 'collection';

                        const exchangeCurrencies = data[enabledExchange];

                        Object.keys(data[enabledExchange]).forEach(function(currency) {

                            if (currency !== 'total_in_euros') {

                                if (currency === 'EUR') {
                                    if (exchangeCurrencies[currency].balance.toFixed(2) > 0) {
                                        const currencyItem = document.createElement('li');
                                        currencyItem.style.width = '100%';
                                        currencyItem.className = 'collection-item';
                                        currencyItem.appendChild(document.createTextNode(currency + ' : ' + exchangeCurrencies[currency].balance.toFixed(2) + '€'));
                                        currencyList.appendChild(currencyItem);
                                    }
                                } else if (currency === 'USD') {
                                    if (exchangeCurrencies[currency].balance.toFixed(2) > 0) {
                                        const currencyItem = document.createElement('li');
                                        currencyItem.style.width = '100%';
                                        currencyItem.className = 'collection-item';
                                        currencyItem.appendChild(document.createTextNode(currency + ' : ' + exchangeCurrencies[currency].balance.toFixed(2) + '€'));
                                        currencyList.appendChild(currencyItem);
                                    }
                                } else {

                                    // If the balance_in_euro of the coin is < 1€ --> I don't show it to the user
                                    if (exchangeCurrencies[currency].balance_in_euros.toFixed(2) > 1) {
                                        const logo = document.createElement('img');
                                        const logoDiv = document.createElement('div');
                                        const holdingsDiv = document.createElement('div');
                                        const currencyItem = document.createElement('li');

                                        currencyItem.style.cursor = 'pointer';

                                        // Showing orders modal - using chartjs
                                        currencyItem.onclick = function() {
                                            $("#ordersModal").modal('open');
                                            $("#ordersModalLoader").show();
                                            $("#currencyMarketOrders").text('');

                                            if (document.getElementById("ordersChart") !== null) {
                                                document.getElementById("ordersChartContainer").removeChild(document.getElementById("ordersChart"));
                                            }

                                            const ordersChartCanvas = document.createElement('canvas');
                                            ordersChartCanvas.id = 'ordersChart';
                                            document.getElementById("ordersChartContainer").appendChild(ordersChartCanvas);

                                            // Destroying previous orders chart if exists
                                            if (ordersHistoryChart !== null) {
                                                ordersHistoryChart.destroy();
                                            }

                                            firebase.auth().currentUser.getIdToken()
                                                .then(function(token) {

                                                    const datacurrencyData = {
                                                        exchange: enabledExchange,
                                                        currency: currency
                                                    };

                                                    $.ajax({
                                                        type: "GET",
                                                        url:"/orders",
                                                        headers: {token : token},
                                                        data: datacurrencyData,
                                                        success: function(resultOrders) {

                                                            for (marketName in resultOrders) {

                                                                const market = resultOrders[marketName];
                                                                const orders = market.orders;
                                                                // const market = market.currency + market.currencyMarket;
                                                                const marketExchangeCurrency = market.currencyMarket;

                                                                const button = document.createElement('a');
                                                                button.className = 'waves-effect waves-light btn';
                                                                button.style.backgroundColor = '#0d47a1';
                                                                button.style.margin = '10px';
                                                                button.text = marketName;
                                                                button.id = marketName;


                                                                $.ajax({
                                                                    type: "GET",
                                                                    url: "https://min-api.cryptocompare.com/data/histohour",
                                                                    data: {
                                                                        fsym: currency,
                                                                        tsym: market.currencyMarket,
                                                                        limit: 16000,
                                                                        aggregate: 3,
                                                                        e: 'CCCAGG'
                                                                    },
                                                                    success: function(historic) {

                                                                        button.onclick = (dataButton) => {

                                                                            $("#currencyMarketOrders > a").each(index => {
                                                                                const button = $("#currencyMarketOrders > a")[index];
                                                                                if (button.id === dataButton.currentTarget.id) {
                                                                                    button.className = 'waves-effect waves-light btn disabled';
                                                                                } else {
                                                                                    button.className = 'waves-effect waves-light btn';
                                                                                }
                                                                            });

                                                                            // here we have orders data and historic price data -> just build the damn graph !
                                                                            const ctx = document.getElementById('ordersChart').getContext('2d');

                                                                            const dataOrdersBuy = [];
                                                                            const dataOrdersSell = [];
                                                                            const dataHistoricLow = [];
                                                                            const dataHistoricHigh = [];

                                                                            // const filledOrdersIndexes = Object.keys(orders).filter(order => orders[order].status === 'FILLED');

                                                                            for (order of orders) {

                                                                                if (order.side === 'BUY'){
                                                                                    dataOrdersBuy.push({
                                                                                        x: new Date(order.time),
                                                                                        y: order.price,
                                                                                        qty: order.quantity
                                                                                    })
                                                                                } else {
                                                                                    dataOrdersSell.push({
                                                                                        x: new Date(order.time),
                                                                                        y: order.price,
                                                                                        qty: order.quantity
                                                                                    })
                                                                                }

                                                                            }

                                                                            for (timeStamp in historic.Data) {
                                                                                dataHistoricLow.push({
                                                                                    x: new Date(historic.Data[timeStamp].time*1000),
                                                                                    y: historic.Data[timeStamp].low
                                                                                });

                                                                                dataHistoricHigh.push({
                                                                                    x: new Date(historic.Data[timeStamp].time*1000),
                                                                                    y: historic.Data[timeStamp].high
                                                                                });
                                                                            }

                                                                            // Chart generation
                                                                            const options = {
                                                                                // Container for pan options
                                                                                pan: {
                                                                                    // Boolean to enable panning
                                                                                    enabled: true,

                                                                                    // Panning directions. Remove the appropriate direction to disable
                                                                                    // Eg. 'y' would only allow panning in the y direction
                                                                                    mode: 'x',
                                                                                    // limits: {
                                                                                    //     min: new Date(Math.min.apply(Math, dataHistoricLow.map(function(o){return o.x;}))),
                                                                                    //     max: new Date(Math.max.apply(Math, dataHistoricLow.map(function(o){return o.x;})))
                                                                                    // }
                                                                                },

                                                                                // Container for zoom options
                                                                                zoom: {
                                                                                    // Boolean to enable zooming
                                                                                    enabled: true,

                                                                                    // Enable drag-to-zoom behavior
                                                                                    drag: false,

                                                                                    // Zooming directions. Remove the appropriate direction to disable
                                                                                    // Eg. 'y' would only allow zooming in the y direction
                                                                                    mode: 'x',
                                                                                    // limits: {
                                                                                    //     max: 10,
                                                                                    //     min: 0.5
                                                                                    // }
                                                                                },
                                                                                scales:
                                                                                    {
                                                                                        xAxes: [{
                                                                                            display: true,
                                                                                            type: 'linear',
                                                                                            ticks: {
                                                                                                maxRotation: 0,
                                                                                                userCallback: function(label, index, labels) {
                                                                                                    return new Date(label).toLocaleDateString('en-EN', {'day': '2-digit', 'month': 'short'});
                                                                                                }
                                                                                            },
                                                                                            afterTickToLabelConversion: function(data) {
                                                                                                const xLabels = data.ticks;
                                                                                                // xLabels.forEach(function (labels, i) {
                                                                                                //     if (i % parseInt(xLabels.length/9) !== 0){
                                                                                                //         xLabels[i] = '';
                                                                                                //     }
                                                                                                // });
                                                                                            }
                                                                                        }],
                                                                                        yAxes: [{
                                                                                            display: true
                                                                                        }]
                                                                                    },
                                                                                tooltips: {
                                                                                    enabled: true,
                                                                                    mode: 'single',
                                                                                    callbacks: {
                                                                                        label: function(tooltipItems, data) {
                                                                                            const point = data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index];

                                                                                            if (point.qty === undefined) {
                                                                                                return point.y + '';
                                                                                            } else {
                                                                                                const tooltipLabel = [point.x];
                                                                                                // tooltipLabel.push(point.y);
                                                                                                // tooltipLabel.push(point.qty + ' ' + currency.toString().toLocaleUpperCase());

                                                                                                return tooltipLabel;
                                                                                            }
                                                                                        },
                                                                                        footer: function(tooltipItems, data) {
                                                                                            if (tooltipItems.length > 0) {
                                                                                                const point = data.datasets[tooltipItems[0].datasetIndex].data[tooltipItems[0].index];
                                                                                                return ['Price: ' + point.y + ' ' + marketExchangeCurrency, 'Qty: ' + point.qty + ' ' + currency.toString().toLocaleUpperCase()];
                                                                                            }
                                                                                        }
                                                                                    },
                                                                                    filter: function(tooltipItem, data) {
                                                                                        return !data.datasets[tooltipItem.datasetIndex].tooltipHidden;
                                                                                    }
                                                                                },
                                                                                layout: {
                                                                                    padding: {
                                                                                        left: 5,
                                                                                        right: 5,
                                                                                        top: 0,
                                                                                        bottom: 0
                                                                                    }
                                                                                }
                                                                            };
                                                                            ordersHistoryChart = new Chart(ctx, {
                                                                                type: 'scatter',
                                                                                data: {
                                                                                    datasets: [{
                                                                                        type: 'scatter',
                                                                                        label: 'Buy',
                                                                                        backgroundColor: "rgb(83, 244, 66)",
                                                                                        pointRadius: 3,
                                                                                        fill: false,
                                                                                        showLine: false,
                                                                                        data: dataOrdersBuy
                                                                                    }, {
                                                                                        type: 'scatter',
                                                                                        label: 'Sell',
                                                                                        backgroundColor: "rgb(255, 29, 0)",
                                                                                        pointRadius: 3,
                                                                                        fill: false,
                                                                                        showLine: false,
                                                                                        data: dataOrdersSell
                                                                                    } , {
                                                                                        type: 'scatter',
                                                                                        label: 'Low',
                                                                                        borderWidth: 1.1,
                                                                                        fill: false,
                                                                                        hoverRadius: 0,
                                                                                        tooltipHidden: true,
                                                                                        data: dataHistoricLow,
                                                                                        pointBorderWidth: 0,
                                                                                        pointHoverRadius: 0,
                                                                                        pointHoverBorderWidth: 0,
                                                                                        pointRadius: 0,
                                                                                        pointHitRadius: 0,
                                                                                        borderColor: 'rgb(66, 134, 244)'
                                                                                    }, {
                                                                                        type: 'scatter',
                                                                                        label: 'High',
                                                                                        borderWidth: 1.1,
                                                                                        pointBorderWidth: 0,
                                                                                        pointHoverRadius: 0,
                                                                                        pointHoverBorderWidth: 0,
                                                                                        pointHitRadius: 0,
                                                                                        tooltipHidden: true,
                                                                                        data: dataHistoricHigh,
                                                                                        pointRadius: 0
                                                                                    }]
                                                                                },
                                                                                options: options
                                                                            });
                                                                        };

                                                                        document.getElementById('currencyMarketOrders').appendChild(button);


                                                                        $("#ordersModalLoader").hide();

                                                                    },
                                                                    error: function(error) {
                                                                        console.log(error);
                                                                    }
                                                                })
                                                            }

                                                        },
                                                        error: function(error) {
                                                            console.log(error);
                                                            $("#ordersModal").modal('close');
                                                            Materialize.toast(error.responseText, 1000);
                                                        }
                                                    });
                                                })
                                                .catch(function(error) {
                                                    console.log(error);
                                                })
                                        };


                                        // trying tradingview charts
                                        // $("#ordersModal").modal('open');
                                        // TradingView.onready(function()
                                        // {
                                        //     var widget = window.tvWidget = new TradingView.widget({
                                        //         fullscreen: true,
                                        //         symbol: 'AAPL',
                                        //         interval: 'D',
                                        //         container_id: "tv_chart_container",
                                        //         //	BEWARE: no trailing slash is expected in feed URL
                                        //         datafeed: new Datafeeds.UDFCompatibleDatafeed("https://demo_feed.tradingview.com"),
                                        //         library_path: "../charting_library/",
                                        //         locale: "en",
                                        //         //	Regression Trend-related functionality is not implemented yet, so it's hidden for a while
                                        //         drawings_access: { type: 'black', tools: [ { name: "Regression Trend" } ] },
                                        //         disabled_features: ["use_localstorage_for_settings"],
                                        //         enabled_features: ["study_templates"],
                                        //         charts_storage_url: 'http://saveload.tradingview.com',
                                        //         charts_storage_api_version: "1.1",
                                        //         client_id: 'tradingview.com',
                                        //         user_id: 'public_user_id'
                                        //     });
                                        //
                                        //     widget.onChartReady(function() {
                                        //         var position = widget.createPositionLine()
                                        //             .onReverse(function(text) {
                                        //             })
                                        //             .onClose(function(text) {
                                        //             })
                                        //             .setText("PROFIT: 71.1 (3.31%)")
                                        //             .setQuantity("8.235")
                                        //             .setLineLength(3);
                                        //         position.setPrice(position.getPrice() - 2);
                                        //
                                        //         var order = widget.createOrderLine()
                                        //             .onCancel(function(text) {
                                        //             })
                                        //             .setText("STOP: 73.5 (5,64%)")
                                        //             .setLineLength(3)
                                        //             .setQuantity("2");
                                        //         order.setPrice(order.getPrice() - 2.5);
                                        //
                                        //         widget.createExecutionShape()
                                        //             .setText("@1,320.75 Limit Buy 1")
                                        //             .setTextColor("rgba(255,0,0,0.5)")
                                        //             .setArrowSpacing(25)
                                        //             .setArrowHeight(25)
                                        //             .setArrowColor("#F00")
                                        //             .setTime(new Date("3 Dec 2014 00:00:00 GMT+0").valueOf() / 1000)
                                        //             .setPrice(15.5);
                                        //     });
                                        //
                                        //     console.log(widget);
                                        // });



                                        let delta = 0;

                                        if (snapshot.val() !== null) {
                                            // Getting the minimum date of all the data
                                            const dateMin = Object.values(snapshot.val()).reduce((min, p) => p.date < min ? p.date : min, Object.values(snapshot.val())[0].date);

                                            // Getting the historic holdings of the last day
                                            const yesterdayHistoricData = Object.values(snapshot.val()).find((obj) => obj.date = dateMin);
                                            // delete yesterdayHistoricData['date'];

                                            let yesterdayExchangeCurrencies = yesterdayHistoricData[enabledExchange];

                                            // To deal with if user didn't have an exchange enabled yesterday error
                                            if (yesterdayExchangeCurrencies === undefined) {
                                                yesterdayExchangeCurrencies = {};
                                                yesterdayExchangeCurrencies[currency] = {};
                                                yesterdayExchangeCurrencies[currency].balance_in_euros = 0;
                                            } else if (yesterdayExchangeCurrencies[currency] === undefined) {
                                                yesterdayExchangeCurrencies[currency] = {};
                                                yesterdayExchangeCurrencies[currency].balance_in_euros = 0;
                                            }

                                            delta = ((exchangeCurrencies[currency].balance_in_euros - yesterdayExchangeCurrencies[currency].balance_in_euros) / ((exchangeCurrencies[currency].balance_in_euros + yesterdayExchangeCurrencies[currency].balance_in_euros) / 2)*100);

                                        } else {
                                            delta = 0;
                                        }

                                        logoDiv.appendChild(logo);

                                        // Sometimes, cryptotompare use different names for coins, i.e. IOTA (is IOT but 'CoinName' property is 'IOTA')
                                        if (cryptocompareData.Data[currency] === undefined) {
                                            const currencyFromCryptocompare = Object.keys(cryptocompareData.Data).filter(cur => {return cryptocompareData.Data[cur].CoinName === currency});
                                            logo.setAttribute('src', baseLogoUrl + cryptocompareData.Data[currencyFromCryptocompare[0]].ImageUrl);
                                        } else {
                                            logo.setAttribute('src', baseLogoUrl + cryptocompareData.Data[currency].ImageUrl);
                                        }

                                        logo.style.width = '100%';
                                        logoDiv.style.width = '12%';
                                        logoDiv.className = 'valign-wrapper';

                                        holdingsDiv.className = 'right-align';
                                        const valuePara = document.createElement('p');
                                        valuePara.style.margin = 0;
                                        const deltaPara = document.createElement('p');
                                        deltaPara.style.margin = 0;

                                        if (delta > 0) {
                                            deltaPara.style.color = 'green';
                                        } else {
                                            deltaPara.style.color = 'red';
                                        }

                                        valuePara.appendChild(document.createTextNode(currency + ' : ' + exchangeCurrencies[currency].balance_in_euros.toFixed(2) + '€'));
                                        deltaPara.appendChild(document.createTextNode('\n' + delta.toFixed(2) + '%'));

                                        holdingsDiv.appendChild(valuePara);
                                        holdingsDiv.appendChild(deltaPara);

                                        holdingsDiv.style.width = '80%';
                                        holdingsDiv.style.paddingTop = '10px';

                                        currencyItem.style.width = '100%';
                                        currencyItem.className = 'collection-item';
                                        currencyItem.style.display = 'inline-flex';
                                        currencyItem.appendChild(logoDiv);
                                        currencyItem.appendChild(holdingsDiv);
                                        currencyList.appendChild(currencyItem);
                                    }
                                }

                            }
                        });

                        body.appendChild(currencyList);
                        li.appendChild(title);
                        li.appendChild(body);
                        ul.appendChild(li);
                    });
                    callback(null);
                });
            },
            error: function(error) {
                console.log(error);

                callback(new Error(error));

            }
        });
    } catch (error) {
        Materialize.toast(error, 2000);
    }



}
