function showSideNavMenu() {
    if ($('#sidenav-overlay').length > 0) {
        $('.button-collapse').sideNav('hide');
    } else {
        $('.button-collapse').sideNav('show');
    }
}