function loadHistoricGraphs() {

    // Deleting actual charts
    var exchangesChartListTabs = document.getElementById("exchangesChartListTabs");
    var exchangesChartListDivs = document.getElementById("exchangesChartListDivs");
    var totalChartListDivs = document.getElementById("totalChartListDivs");

    if (exchangesChartListTabs !== null) {
        while (exchangesChartListTabs.firstChild) {
            exchangesChartListTabs.removeChild(exchangesChartListTabs.firstChild);
        }
    }

    if (exchangesChartListDivs !== null) {
        while (exchangesChartListDivs.firstChild) {
            exchangesChartListDivs.removeChild(exchangesChartListDivs.firstChild);
        }
    }

    if (totalChartListDivs !== null) {
        while (totalChartListDivs.firstChild) {
            totalChartListDivs.removeChild(totalChartListDivs.firstChild);
        }
    }
    //

    firebase.database().ref(firebase.auth().currentUser.uid).on('value', function(snapshot) {

            // Using a webworker to create data charts -> CPU consuming action
            const exchangesWorker = new Worker("js/workers/chart-workers/exchanges-chart-worker.js");
            const holdingsWorker = new Worker("js/workers/chart-workers/holdings-historic-worker.js");

            if (snapshot.hasChild('config')) {
                //Creating list and divs charts for enabled exchanges
                const enabledExchanges = Object.keys(snapshot.val().config.exchanges).filter(exchange => snapshot.val().config.exchanges[exchange].enabled == true);

                if (enabledExchanges.length > 0) {
                    // Holdings historic part
                    holdingsWorker.postMessage(snapshot.val().historic);
                    holdingsWorker.onmessage = function(holdingsHistoricChartData) {

                        const holdingsChartDiv = document.getElementById('totalChartListDivs');

                        if (holdingsHistoricChartData.data.data !== undefined) {

                            const canvas = document.createElement("canvas");
                            canvas.id = 'totalHistoricChart';
                            holdingsChartDiv.appendChild(canvas);

                            const ctx = document.getElementById('totalHistoricChart').getContext('2d');

                            const options = {
                                scales:
                                    {
                                        xAxes: [{
                                            display: true,
                                            type: 'linear',
                                            ticks: {
                                                maxRotation: 0,
                                                userCallback: function(label, index, labels) {
                                                    return new Date(label).toLocaleDateString('en-EN', {'day': '2-digit', 'month': 'short'});
                                                }
                                            },
                                            afterTickToLabelConversion: function(data) {
                                                const xLabels = data.ticks;
                                                // xLabels.forEach(function (labels, i) {
                                                //     if (i % parseInt(xLabels.length/9) !== 0){
                                                //         xLabels[i] = '';
                                                //     }
                                                // });
                                            }
                                        }],
                                        yAxes: [{
                                            scaleLabel: {
                                                display: true,
                                                labelString: '€'
                                            }
                                        }]
                                    },
                                tooltips: {
                                    enabled: true,
                                    mode: 'single',
                                    callbacks: {
                                        label: function(tooltipItems, data) {
                                            const point = data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index];
                                            return point.y.toFixed(2) + ' €';
                                        },
                                        title: function (tooltipItem, data) {
                                            const moment = new Date(tooltipItem[0].xLabel);
                                            return moment.toLocaleDateString('en-EN', {'day': '2-digit', 'month': 'short'}) + ' - ' + moment.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});
                                        }
                                    }
                                },
                                elements: {
                                    point: {
                                        radius: 0,
                                        hitRadius: 10,
                                        hoverRadius: 5
                                    }
                                },
                                legend: {
                                    display: false
                                },
                                gridLines: {
                                    drawTicks: false
                                }
                            };

                            const chart = new Chart(ctx, {
                                type: 'line',
                                data: {
                                    datasets: [{
                                        label: 'Holdings historic',
                                        data: holdingsHistoricChartData.data.data
                                    }],
                                    labels: holdingsHistoricChartData.data.labels
                                },
                                options: options
                            });
                        } else {
                            holdingsChartDiv.appendChild(document.createTextNode("No historic data found - please wait some hours"));
                        }
                        $("#totalHistoricGraphLoader").hide();

                    };


                    // Exchanges historic part
                    enabledExchanges.forEach(function(enabledExchange) {

                        let dataToPostToExchangesWorker = {
                            exchange: enabledExchange,
                            historic: snapshot.val().historic
                        };

                        exchangesWorker.postMessage(dataToPostToExchangesWorker);

                        exchangesWorker.onmessage = function(exchangesChartsData) {

                            var exchange = exchangesChartsData.data.exchange;

                            // Building the tabds of exchanges charts
                            var ul = document.getElementById("exchangesChartListTabs");
                            var li = document.createElement("li");
                            li.className = 'tab';
                            var a = document.createElement("a");
                            a.href = '#' + exchange + 'HistoricChartDiv';
                            a.appendChild(document.createTextNode(exchange));
                            li.appendChild(a);
                            ul.appendChild(li);

                            // Building the divs of exchanges charts
                            var mainDiv = document.getElementById("exchangesChartListDivs");
                            var childDiv = document.createElement("div");
                            childDiv.id = exchange + 'HistoricChartDiv';

                            // Initialize tabs
                            $(document).ready(function(){
                                $('ul.tabs').tabs();
                            });


                            if (exchangesChartsData.data[exchange] !== undefined) {

                                var canvas = document.createElement("canvas");
                                canvas.id = exchange + 'HistoricChart';
                                childDiv.appendChild(canvas);
                                mainDiv.appendChild(childDiv);

                                var ctx = document.getElementById(exchange + 'HistoricChart').getContext('2d');

                                var options = {
                                    scales:
                                        {
                                            xAxes: [{
                                                display: true,
                                                type: 'linear',
                                                ticks: {
                                                    maxRotation: 0,
                                                    userCallback: function(label, index, labels) {
                                                        return new Date(label).toLocaleDateString('en-EN', {'day': '2-digit', 'month': 'short'});
                                                    }
                                                },
                                                afterTickToLabelConversion: function(data) {
                                                    const xLabels = data.ticks;
                                                    // xLabels.forEach(function (labels, i) {
                                                    //     if (i % parseInt(xLabels.length/9) !== 0){
                                                    //         xLabels[i] = '';
                                                    //     }
                                                    // });
                                                }
                                            }],
                                            yAxes: [{
                                                scaleLabel: {
                                                    display: true,
                                                    labelString: '€'
                                                }
                                            }]
                                        },
                                    tooltips: {
                                        enabled: true,
                                        mode: 'single',
                                        callbacks: {
                                            label: function(tooltipItems, data) {
                                                const point = data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index];
                                                return point.y + ' €';
                                                },
                                            title: function (tooltipItem, data) {
                                                const moment = new Date(tooltipItem[0].xLabel);
                                                return moment.toLocaleDateString('en-EN', {'day': '2-digit', 'month': 'short'}) + ' - ' + moment.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});
                                            }
                                            }
                                    },
                                    elements: {
                                        point: {
                                            radius: 0,
                                            hitRadius: 10,
                                            hoverRadius: 5
                                        }
                                    },
                                    legend: {
                                        display: false
                                    }
                                } ;

                                var chart = new Chart(ctx, {
                                    type: 'line',
                                    data: {
                                        datasets: [{
                                            label: exchange + ' historic',
                                            data: exchangesChartsData.data[exchange].data
                                        }],
                                        labels: exchangesChartsData.data[exchange].labels
                                    },
                                    options: options
                                });
                            } else {

                                childDiv.appendChild(document.createTextNode("No historic data found - please wait some hours"));
                                mainDiv.appendChild(childDiv);
                            }

                            $("#exchangesHistoricGraphLoader").hide();
                        };
                    });
                } else {
                    var mainDiv = document.getElementById("exchangesChartListDivs");
                    mainDiv.appendChild(document.createTextNode("No exchange is enabled - Please enable some"));
                    $("#exchangesHistoricGraphLoader").hide();
                }
            } else {
                goToExchanges();
                Materialize.toast("First, enter some exchanges API keys (only those in read mode !)", 5000);
            }
        });
}