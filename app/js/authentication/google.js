function googleAuthenticate(callback) {
    const provider = new firebase.auth.GoogleAuthProvider();

    firebase.auth().signInWithRedirect(provider);

    // firebase.auth().signInWithPopup(provider).then(function(result) {
    //     // storeUserUid(result.user.uid);
    //
    //     callback(null, result.user);
    //
    // }).catch(function(error) {
    //     if (error.code === 'auth/account-exists-with-different-credential') {
    //
    //         // User's email already exists in here
    //
    //         // Checking with which provider the user is already signed in
    //         firebase.auth().fetchProvidersForEmail(error.email)
    //             .then(function(providers) {
    //                 switch (providers[0]) {
    //                     case 'google.com':
    //                         customizeModalAndPassCredentials(error.email, providers[0], error.credential.providerId, error);
    //                         break;
    //                     case 'twitter.com':
    //                         customizeModalAndPassCredentials(error.email, providers[0], error.credential.providerId, error);
    //                         break;
    //                     case 'github.com':
    //                         customizeModalAndPassCredentials(error.email, providers[0], error.credential.providerId, error);
    //                         break;
    //                     case 'facebook.com':
    //                         customizeModalAndPassCredentials(error.email, providers[0], error.credential.providerId, error);
    //                         break;
    //
    //                     default:
    //                         console.log('No other provider found - TODO : Handle that correctly');
    //                         break;
    //                 }
    //             })
    //             .catch(function(error) {
    //                 console.log(error);
    //                 Materialize.toast(error.message, 3000);
    //             });
    //
    //     }
    // });
}

function googleSignOut() {
    $('.button-collapse').sideNav('hide');
    firebase.auth().signOut().then(function() {
        console.log('Signed Out');
    }, function(error) {
        console.error('Sign Out Error', error);
    });
}