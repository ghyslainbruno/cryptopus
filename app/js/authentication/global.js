function signInExistantUserWithGlobalProvider() {

    let provider = null;

    switch (sessionStorage.getItem('providerToLink')) {
        case 'google.com':
            const googleProvider = new firebase.auth.GoogleAuthProvider();
            provider = googleProvider;

            break;
        case 'twitter.com':
            const twitterProvider = new firebase.auth.TwitterAuthProvider();
            provider = twitterProvider;

            break;
        case 'github.com':
            const githubProvider = new firebase.auth.GithubAuthProvider();
            provider = githubProvider;

            break;
        case 'facebook.com':
            const facebookProvider = new firebase.auth.FacebookAuthProvider();
            provider = facebookProvider;

            break;
        default:
            break;
    }

    // TODO: handle that in signInWithRedirect way (Google preconization)
    sessionStorage.setItem('isAccountToLink', true);
    firebase.auth().signInWithRedirect(provider);
}

function customizeModalAndPassCredentials(email, providerToLink, globalProvider, error) {
    // Filling the modal with user information
    $("#userEmail").text(email);
    $(".knownProvider").text(providerToLink);
    $("#provider").text(globalProvider);

    // displaying the modal to the user
    $("#userAlreadyExistsModal").modal('open');

    // Trying to use sessionStorage (as preconized by firebase) instead of global variables to
    // deal with redirect auth instead of popup auth
    // (as preconized by firebase for mobile applications)
    sessionStorage.setItem('globalProvider', globalProvider);
    sessionStorage.setItem('providerToLink', providerToLink);

    //// Tried to use sessionStorage to pass pendingCredentials with no success
    sessionStorage.setItem('pendingCredentials', JSON.stringify(error.credential));
}

function storeUserUid(uid) {

    firebase.database().ref("users/list").once("value").
        then(function(snapshot) {

            if (snapshot.val() !== null) {

                firebase.database().ref("users/list/" + snapshot.val().length).set(uid)
                    .then(function(result) {
                        console.log(result);
                    })
                    .catch(function(error) {
                        console.log(error);
                    })

            } else {
                firebase.database().ref("users/list/1").set(uid)
                    .then(function(result) {
                        console.log(result);
                    })
                    .catch(function(error) {
                        console.log(error);
                    })
            }

        })
        .catch(function(error) {

        });
}

function checkForRedirectResult() {
    firebase.auth().getRedirectResult()
        .then(function(result) {

            // Here we check if we want to link 2 accounts providers / if not -> nothing is done
            if (sessionStorage.getItem('isAccountToLink')) {
                // console.log("should be linked");

                let credentials = {};

                switch (sessionStorage.getItem('globalProvider')) {
                    case 'google.com':
                        credentials = firebase.auth.GoogleAuthProvider.credential(JSON.parse(sessionStorage.getItem('pendingCredentials')).accessToken);

                        break;
                    case 'twitter.com':
                        credentials = firebase.auth.TwitterAuthProvider.credential(JSON.parse(sessionStorage.getItem('pendingCredentials')).accessToken);

                        break;
                    case 'github.com':
                        credentials = firebase.auth.GithubAuthProvider.credential(JSON.parse(sessionStorage.getItem('pendingCredentials')).accessToken);

                        break;
                    case 'facebook.com':
                        credentials = firebase.auth.FacebookAuthProvider.credential(JSON.parse(sessionStorage.getItem('pendingCredentials')).accessToken);

                        break;
                    default:
                        break;
                }

                result.user.linkWithCredential(credentials)
                    .then(function(test) {
                        Materialize.toast("Accounts successfully linked !", 1000);
                        sessionStorage.removeItem('isAccountToLink');
                        sessionStorage.removeItem('globalProvier');
                        sessionStorage.removeItem('providerToLink');
                        sessionStorage.removeItem('pendingCredentials');
                        // sessionStorage.clear();
                    })
                    .catch(function(error) {
                        // sessionStorage.clear();
                        sessionStorage.removeItem('isAccountToLink');
                        sessionStorage.removeItem('globalProvier');
                        sessionStorage.removeItem('providerToLink');
                        sessionStorage.removeItem('pendingCredentials');
                        Materialize.toast("The was an error during accounts linking", 1000);
                })
            }
        })

        .catch(function(error) {
            if (error.code === 'auth/account-exists-with-different-credential') {

                // User's email already exists in here

                // Checking with which provider the user is already signed in
                firebase.auth().fetchProvidersForEmail(error.email)
                    .then(function(providers) {
                        switch (providers[0]) {
                            case 'google.com':
                                customizeModalAndPassCredentials(error.email, providers[0], error.credential.providerId, error);
                                break;
                            case 'twitter.com':
                                customizeModalAndPassCredentials(error.email, providers[0], error.credential.providerId, error);
                                break;
                            case 'github.com':
                                customizeModalAndPassCredentials(error.email, providers[0], error.credential.providerId, error);
                                break;
                            case 'facebook.com':
                                customizeModalAndPassCredentials(error.email, providers[0], error.credential.providerId, error);
                                break;

                            default:
                                console.log('No other provider found - TODO : Handle that correctly');
                                break;
                        }
                    })
                    .catch(function(error) {
                        console.log(error);
                        Materialize.toast(error.message, 3000);
                    });

            }
        });
}