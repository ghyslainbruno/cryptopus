function responseToVote() {
    // const $toastContent = $('<span>I am toast content</span>').add($('<button class="btn-flat toast-action">Undo</button>'));
    // Materialize.toast($toastContent, 3000);

    Materialize.toast("Voted", 1000);
}

function firstFeat() {
    vote('all_currencies_chart');
    getNumberOfVotes();
}

function secondFeat() {
    vote('change_eur_btc');
    getNumberOfVotes();
}

function thirdFeat() {
    vote('night_mode');
    getNumberOfVotes();
}

function fourthFeat() {
    vote('kucoin');
    getNumberOfVotes();
}

function fifthFeat() {
    vote('huobi');
    getNumberOfVotes();
}
function nextFeaturesCardUndo() {
    Materialize.toast("Votes for next features removed", 1000);
}

function apiSupportsUndo() {
    Materialize.toast("Votes for API supports removed", 1000);
}

function getNumberOfVotes() {
    firebase.database().ref(firebase.auth().currentUser.uid + '/votes').once('value')
        .then(function(snapshot) {
            console.log(snapshot.val());
        })
        .catch(function(error){
            if (error) {
                console.log(error);
            } else {

            }
        })
}

function vote(feature) {

    const uid = firebase.auth().currentUser.uid;

    firebase.database().ref(firebase.auth().currentUser.uid + '/votes').once('value')
        .then(function(hasUserVoted) {

            // user doesn't have (at least) "votes" node
            if (hasUserVoted.val() === null) {
                firebase.database().ref('votes').once('value')
                    .then(function(snapshot) {

                        let voteNumber = 0;

                        if (snapshot.val()[feature] !== undefined) {
                            // Getting the total of votes here and make a +1 (or -1 later)
                            voteNumber = snapshot.val()[feature];
                        }

                        firebase.database().ref('votes/' + feature ).set(voteNumber + 1)
                            .then(function(result) {
                                console.log(result);
                            })
                            .catch(function(error) {
                                console.log(error);
                            });

                        firebase.database().ref(uid + '/votes/').update({[feature]: true})
                            .then(function (result) {
                                Materialize.toast("Voted", 1000);
                                console.log(result);
                            })
                            .catch(function (error) {
                                console.log(error);
                            })

                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            } else {
                if (hasUserVoted.val()[feature]) {

                    firebase.database().ref('votes').once('value')
                        .then(function(snapshot) {

                            if (snapshot.val()[feature] !== undefined && snapshot.val()[feature] > 0) {
                                // Getting the total of votes here and make a +1 (or -1 later)
                                let voteNumber = snapshot.val()[feature];

                                // Decrement for all users
                                firebase.database().ref('votes/' + feature ).set(voteNumber - 1)
                                    .then(function(result) {
                                        console.log(result);
                                    })
                                    .catch(function(error) {
                                        console.log(error);
                                    });

                                // Remove vote in user database
                                firebase.database().ref(uid + '/votes/').update({[feature]: false})
                                    .then(function (result) {
                                        console.log(result);
                                        Materialize.toast("Vote removed", 1000);
                                    })
                                    .catch(function (error) {
                                        console.log(error);
                                    })

                            } else {
                                Materialize.toast("Cannot remove vote if no one has already voted", 1000);
                            }
                        })
                        .catch(function(error) {
                            console.log(error);
                        });

                } else {
                    firebase.database().ref('votes').once('value')
                        .then(function(snapshot) {

                            let voteNumber = 0;

                            if (snapshot.val()[feature] !== undefined) {
                                // Getting the total of votes here and make a +1 (or -1 later)
                                voteNumber = snapshot.val()[feature];
                            }

                            firebase.database().ref('votes/' + feature ).set(voteNumber + 1)
                                .then(function(result) {
                                    console.log(result);
                                })
                                .catch(function(error) {
                                    console.log(error);
                                });

                            firebase.database().ref(uid + '/votes/').update({[feature]: true})
                                .then(function (result) {
                                    Materialize.toast("Voted", 1000);
                                    console.log(result);
                                })
                                .catch(function (error) {
                                    console.log(error);
                                })

                        })
                        .catch(function(error) {
                            console.log(error);
                        });
                }
            }
        })
        .catch(function(error) {
            console.log(error);
        });
}

function attachVotesListeners() {

    // All votes count
    firebase.database().ref(firebase.auth().currentUser.uid + '/votes').on('value', function(snapshot) {

        let totalVotes = 0;

        for (let isFeatureVoted in snapshot.val()) {
            if (snapshot.val()[isFeatureVoted]) {
                totalVotes++;
            }
        }

        $("#numberOfVotes").text(totalVotes + '/5');
    });

    // First feature
        // Color of thum
    firebase.database().ref(firebase.auth().currentUser.uid + '/votes/all_currencies_chart').on('value', function(snapshot) {
        if (snapshot.val()) {
            $("#allCurrenciesChartThumb")[0].style.color = '#1e88e5';
        } else {
            $("#allCurrenciesChartThumb")[0].style.color = 'lightGrey';
        }
    });
        // Number chip
    firebase.database().ref('votes').on('value',function(snapshot){
            $("#allCurrenciesChartChip").text(snapshot.val().all_currencies_chart);
    });
    //


    // Second feature
    // Color of thum
    firebase.database().ref(firebase.auth().currentUser.uid + '/votes/change_eur_btc').on('value', function(snapshot) {
        if (snapshot.val()) {
            $("#changeCurrencyThumb")[0].style.color = '#1e88e5';
        } else {
            $("#changeCurrencyThumb")[0].style.color = 'lightGrey';
        }
    });
    // Number chip
    firebase.database().ref('votes').on('value',function(snapshot){
        $("#changeCurrencyChip").text(snapshot.val().change_eur_btc);
    });
    //


    // Third feature
    // Color of thum
    firebase.database().ref(firebase.auth().currentUser.uid + '/votes/night_mode').on('value', function(snapshot) {
        if (snapshot.val()) {
            $("#nightModeThumb")[0].style.color = '#1e88e5';
        } else {
            $("#nightModeThumb")[0].style.color = 'lightGrey';
        }
    });
    // Number chip
    firebase.database().ref('votes').on('value',function(snapshot){
        $("#nightModeChip").text(snapshot.val().night_mode);
    });
    //


    // Fourth feature
    // Color of thum
    firebase.database().ref(firebase.auth().currentUser.uid + '/votes/kucoin').on('value', function(snapshot) {
        if (snapshot.val()) {
            $("#kucoinThumb")[0].style.color = '#1e88e5';
        } else {
            $("#kucoinThumb")[0].style.color = 'lightGrey';
        }
    });
    // Number chip
    firebase.database().ref('votes').on('value',function(snapshot){
        $("#kucoinChip").text(snapshot.val().kucoin);
    });
    //

    // Fifth feature
    // Color of thum
    firebase.database().ref(firebase.auth().currentUser.uid + '/votes/huobi').on('value', function(snapshot) {
        if (snapshot.val()) {
            $("#huobiThumb")[0].style.color = '#1e88e5';
        } else {
            $("#huobiThumb")[0].style.color = 'lightGrey';
        }
    });
    // Number chip
    firebase.database().ref('votes').on('value',function(snapshot){
        $("#huobiChip").text(snapshot.val().huobi);
    });
    //
}
