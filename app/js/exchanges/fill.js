let isGdaxSwitchlistenerAttached = false;
let isBinanceSwitchlistenerAttached = false;
let isBittrexSwitchlistenerAttached = false;


function fillExchangesApi() {

    const user = firebase.auth().currentUser;
    const database = firebase.database();

    // Attaching listeners to switches
    //// Listening for changes from client
    attachGdaxSwitchListener(user, database);
    attachBinanceSwitchListener(user, database);
    attachBittrexListener(user, database);

    //// Listening changes from database
    database.ref(user.uid + "/config/exchanges/gdax/enabled").on('value', function(enabled) {
        if (enabled.val()) {
            $("#gdaxEnabled").prop("checked", true);
        } else {
            $("#gdaxEnabled").prop("checked", false);
        }
    });

    database.ref(user.uid + "/config/exchanges/bittrex/enabled").on('value', function(enabled) {
        if (enabled.val()) {
            $("#bittrexEnabled").prop("checked", true);
        } else {
            $("#bittrexEnabled").prop("checked", false);
        }
    });

    database.ref(user.uid + "/config/exchanges/binance/enabled").on('value', function(enabled) {
        if (enabled.val()) {
            $("#binanceEnabled").prop("checked", true);
        } else {
            $("#binanceEnabled").prop("checked", false);
        }
    });


    // Fill with fake values

    // GDAX part
    // $("#gdaxApiKey").val('\u2022\u2022\u2022\u2022\u2022\u2022');
    // $("#gdaxSecret").val('\u2022\u2022\u2022\u2022\u2022\u2022');
    // $("#gdaxPassPhrase").val('\u2022\u2022\u2022\u2022\u2022\u2022');
    // $("#labelGdaxApiKey").addClass('active');
    // $("#labelGdaxPassphrase").addClass('active');
    // $("#labelGdaxSecret").addClass('active');

    // Binance part
    // $("#binanceApiKey").val('\u2022\u2022\u2022\u2022\u2022\u2022');
    // $("#binanceSecret").val('\u2022\u2022\u2022\u2022\u2022\u2022');
    // $("#labelBinanceApiKey").addClass('active');
    // $("#labelBinanceSecret").addClass('active');

    // Bittrex part
    // $("#bittrexApiKey").val('\u2022\u2022\u2022\u2022\u2022\u2022');
    // $("#bittrexSecret").val('\u2022\u2022\u2022\u2022\u2022\u2022');
    // $("#labelBittrexSecret").addClass('active');
    // $("#labelBittrexApiKey").addClass('active');

    database.ref(user.uid + "/config/exchanges").once('value')
        .then(function(snapshot) {

            // GDAX part
            if (snapshot.val().gdax !== undefined) {
                $("#gdaxApiKey").val('\u2022\u2022\u2022\u2022' + snapshot.val().gdax.api_key.substr(snapshot.val().gdax.api_key.length -4 ));
                $("#gdaxSecret").val('\u2022\u2022\u2022\u2022' + snapshot.val().gdax.secret.substr(snapshot.val().gdax.secret.length- 4));
                $("#gdaxPassPhrase").val('\u2022\u2022\u2022\u2022' + snapshot.val().gdax.pass_phrase.substr(snapshot.val().gdax.pass_phrase.length-4 ));

                $("#labelGdaxApiKey").addClass('active');
                $("#labelGdaxPassphrase").addClass('active');
                $("#labelGdaxSecret").addClass('active');

            }


            // Binance part
            if (snapshot.val().binance !== undefined) {
                $("#binanceApiKey").val('\u2022\u2022\u2022\u2022' + snapshot.val().binance.api_key.substr(snapshot.val().binance.api_key.length - 4));
                $("#binanceSecret").val('\u2022\u2022\u2022\u2022' + snapshot.val().binance.secret.substr(snapshot.val().binance.secret.length - 4));

                $("#labelBinanceApiKey").addClass('active');
                $("#labelBinanceSecret").addClass('active');

            }


            // Bittrex part
            if (snapshot.val() !== undefined) {
                $("#bittrexApiKey").val('\u2022\u2022\u2022\u2022' + snapshot.val().bittrex.api_key.substr(snapshot.val().bittrex.api_key.length - 4));
                $("#bittrexSecret").val('\u2022\u2022\u2022\u2022' + snapshot.val().bittrex.secret.substr(snapshot.val().bittrex.secret.length - 4));

                $("#labelBittrexSecret").addClass('active');
                $("#labelBittrexApiKey").addClass('active');

            }

        })
        .catch(function(error) {
            console.log(error);
        });
}


function attachGdaxSwitchListener(user, database) {

    if (!isGdaxSwitchlistenerAttached) {

        isGdaxSwitchlistenerAttached = true;

        document.getElementById('gdaxEnabled').addEventListener('change', function(event) {

            database.ref(user.uid + "/config/exchanges/gdax").once('value')
                .then(function(snapshot) {

                    if (snapshot.val() !== null) {
                        // if undefined -> so user never stored api keys -> don't enable the exchange
                        if (snapshot.val().api_key == undefined && snapshot.val().secret == undefined && snapshot.val().pass_phrase == undefined) {

                            if ($("#gdaxEnabled").is(':checked')) {
                                $("#gdaxEnabled").prop("checked", false);
                            } else {
                                $("#gdaxEnabled").prop("checked", true);
                            }
                            Materialize.toast('Please enter some valid keys first', 2000);

                        } else {
                            database.ref(user.uid + "/config/exchanges/gdax/enabled").set($('#gdaxEnabled').is(':checked'))
                                .then(function() {
                                    if ($('#gdaxEnabled').is(':checked')) {
                                        Materialize.toast('GDAX enabled', 1000);
                                    } else {
                                        Materialize.toast('GDAX disabled', 1000);
                                    }
                                })
                                .catch(function(error) {
                                    console.log(error);
                                })
                        }
                    } else {
                        if ($("#gdaxEnabled").is(':checked')) {
                            $("#gdaxEnabled").prop("checked", false);
                        } else {
                            $("#gdaxEnabled").prop("checked", true);
                        }
                        Materialize.toast('Please enter some valid keys first', 2000);
                    }


                });
        });
    }


}

function attachBinanceSwitchListener(user, database) {
    if (!isBinanceSwitchlistenerAttached) {

        isBinanceSwitchlistenerAttached = true;

        document.getElementById('binanceEnabled').addEventListener('change', function(event) {

            database.ref(user.uid + "/config/exchanges/binance").once('value')
                .then(function(snapshot) {

                    if (snapshot.val() !== null) {
                        if (snapshot.val().api_key == undefined && snapshot.val().secret == undefined == undefined) {

                            if ($("#binanceEnabled").is(':checked')) {
                                $("#binanceEnabled").prop("checked", false);
                            } else {
                                $("#binanceEnabled").prop("checked", true);
                            }
                            Materialize.toast('Please enter some valid keys first', 2000);

                        } else {
                            database.ref(user.uid + "/config/exchanges/binance/enabled").set($('#binanceEnabled').is(':checked'))
                                .then(function() {
                                    if ($('#binanceEnabled').is(':checked')) {
                                        Materialize.toast('Binance enabled', 1000);
                                    } else {
                                        Materialize.toast('Binance disabled', 1000);
                                    }
                                })
                                .catch(function(error) {
                                    console.log(error);
                                })
                        }
                    } else {
                        if ($("#binanceEnabled").is(':checked')) {
                            $("#binanceEnabled").prop("checked", false);
                        } else {
                            $("#binanceEnabled").prop("checked", true);
                        }
                        Materialize.toast('Please enter some valid keys first', 2000);
                    }
                });
        });
    }

}

function attachBittrexListener(user, database) {

    if (!isBittrexSwitchlistenerAttached) {

        isBittrexSwitchlistenerAttached = true;

        document.getElementById('bittrexEnabled').addEventListener('change', function(event) {

            database.ref(user.uid + "/config/exchanges/bittrex").once('value')
                .then(function(snapshot) {

                    if (snapshot.val() !== null) {
                        if (snapshot.val().api_key == undefined && snapshot.val().secret == undefined == undefined) {

                            if ($("#bittrexEnabled").is(':checked')) {
                                $("#bittrexEnabled").prop("checked", false);
                            } else {
                                $("#bittrexEnabled").prop("checked", true);
                            }
                            Materialize.toast('Please enter some valid keys first', 2000);

                        } else {
                            database.ref(user.uid + "/config/exchanges/bittrex/enabled").set($('#bittrexEnabled').is(':checked'))
                                .then(function() {
                                    if ($('#bittrexEnabled').is(':checked')) {
                                        Materialize.toast('Bittrex enabled', 1000);
                                    } else {
                                        Materialize.toast('Bittrex disabled', 1000);
                                    }
                                })
                                .catch(function(error) {
                                    console.log(error);
                                })
                        }
                    } else {
                        if ($("#bittrexEnabled").is(':checked')) {
                            $("#bittrexEnabled").prop("checked", false);
                        } else {
                            $("#bittrexEnabled").prop("checked", true);
                        }
                        Materialize.toast('Please enter some valid keys first', 2000);
                    }



                });


        });
    }


}