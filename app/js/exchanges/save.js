function saveGdaxExchange() {

    $("#saveGdax").addClass('disabled');

    const database = firebase.database();

    const user = firebase.auth().currentUser;

    const data = {};
    // data.enabled = $('#gdaxEnabled').is(':checked');
    data.api_key = $("[name='gdaxApiKey']").val();
    data.pass_phrase = $("[name='gdaxPassPhrase']").val();
    data.secret = $("[name='gdaxSecret']").val();
    data.exchange = "gdax";

    $.ajax({
        type: "GET",
        url:"/validateApiKeys",
        data: data,
        success: function(dataFromBack) {
            console.log(dataFromBack);
            delete data['exchange'];

            //change values for the encrypted ones
            data['api_key'] = dataFromBack.apiKey;
            data['secret'] = dataFromBack.secret;
            data['pass_phrase'] = dataFromBack.passPhrase;

            database.ref(user.uid + "/config/exchanges/gdax/").update(data)
                .then(function() {
                $("[name='gdaxApiKey']").val('\u2022\u2022\u2022\u2022' + dataFromBack.apiKey.substr(dataFromBack.apiKey.length - 4));
                $("[name='gdaxSecret']").val('\u2022\u2022\u2022\u2022' + dataFromBack.secret.substr(dataFromBack.secret.length - 4));
                $("[name='gdaxPassPhrase']").val('\u2022\u2022\u2022\u2022' + dataFromBack.passPhrase.substr(dataFromBack.passPhrase.length - 4));
                Materialize.toast("Encrypted and Saved", 1000);
                $("#saveGdax").removeClass('disabled');
            });
        },
        error: function(error) {
            Materialize.toast(error.responseText, 1000);
            $("[name='gdaxApiKey']").addClass('invalid');
            $("[name='gdaxPassPhrase']").addClass('invalid');
            $("[name='gdaxSecret']").addClass('invalid');
            $("#saveGdax").removeClass('disabled');
        }
    });
}

function saveBinanceExchange() {
    $("#saveBinance").addClass('disabled');

    const database = firebase.database();

    const user = firebase.auth().currentUser;

    const data = {};
    // data.enabled = $('#binanceEnabled').is(':checked');
    data.api_key = $("[name='binanceApiKey']").val();
    data.secret = $("[name='binanceSecret']").val();
    data.exchange = "binance";

    $.ajax({
        type: "GET",
        url:"/validateApiKeys",
        data: data,
        success: function(dataFromBack) {
            console.log(dataFromBack);
            delete data['exchange'];

            //change values for the encrypted ones
            data['api_key'] = dataFromBack.apiKey;
            data['secret'] = dataFromBack.secret;

            database.ref(user.uid + "/config/exchanges/binance/").update(data).then(function() {
                $("[name='binanceApiKey']").val('\u2022\u2022\u2022\u2022' + dataFromBack.apiKey.substr(dataFromBack.apiKey.length - 4));
                $("[name='binanceSecret']").val('\u2022\u2022\u2022\u2022' + dataFromBack.secret.substr(dataFromBack.secret.length - 4));
                Materialize.toast("Encrypted and Saved", 1000);
                $("#saveBinance").removeClass('disabled');
            });
        },
        error: function(error) {
            Materialize.toast(error.responseText, 1000);
            $("[name='binanceApiKey']").addClass('invalid');
            $("[name='binanceSecret']").addClass('invalid');
            $("#saveBinance").removeClass('disabled');
        }
    })
}

function saveBittrexExchange() {
    $("#saveBittrex").addClass('disabled');

    const database = firebase.database();

    const user = firebase.auth().currentUser;

    const data = {};
    // data.enabled = $('#bittrexEnabled').is(':checked');
    data.api_key = $("[name='bittrexApiKey']").val();
    data.secret = $("[name='bittrexSecret']").val();
    data.exchange = "bittrex";

    $.ajax({
        type: "GET",
        url:"/validateApiKeys",
        data: data,
        success: function(dataFromBack) {
            console.log(dataFromBack);
            delete data['exchange'];

            //change values for the encrypted ones
            data['api_key'] = dataFromBack.apiKey;
            data['secret'] = dataFromBack.secret;

            database.ref(user.uid + "/config/exchanges/bittrex/").update(data).then(function() {
                $("[name='bittrexApiKey']").removeClass('invalid');
                $("[name='bittrexSecret']").removeClass('invalid');
                $("[name='bittrexApiKey']").addClass('valid');
                $("[name='bittrexApiKey']").addClass('valid');

                // Filling the forms with encrypted keys
                $("[name='bittrexApiKey']").val('\u2022\u2022\u2022\u2022' + dataFromBack.apiKey.substr(dataFromBack.secret.length - 4));
                $("[name='bittrexSecret']").val('\u2022\u2022\u2022\u2022' + dataFromBack.secret.substr(dataFromBack.secret.length - 4));

                Materialize.toast("Encrypted and Saved", 1000);
                $("#saveBittrex").removeClass('disabled');
            });
        },
        error: function(error) {
            Materialize.toast(error.responseText, 1000);
            $("[name='bittrexApiKey']").addClass('invalid');
            $("[name='bittrexSecret']").addClass('invalid');
            $("#saveBittrex").removeClass('disabled');
        }
    })
}