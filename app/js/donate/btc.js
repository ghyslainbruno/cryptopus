function copyWalletAddress(currency) {

    let addressToCopy = {};

    switch (currency) {
        case 'btc':
            addressToCopy = '172CVLX5cyQeo8GwPB21Ru5XfpmYw4tUg2';
            break;
        case 'ltc':
            addressToCopy = 'LMVRzyDzzwguB86BhBWzosmcqSmWRiSoDY';
            break;
        case 'eth':
            addressToCopy = '0x71e309573df7ce555c667ab714497cfdcfd5735c';
            break;

        default:
            break;
    }

    // Kinda hack/workaround to copy to clipboard value which is not inside an input/textArea element -> dirty
    const inputElement = document.createElement('input');
    document.body.appendChild(inputElement);
    inputElement.setAttribute('id', 'inputElementId');
    document.getElementById('inputElementId').value = addressToCopy;
    inputElement.select();

    document.execCommand("Copy");

    document.getElementById('inputElementId').remove();

    // $("#donateModal").modal('close');
    // hideSideNav();
    Materialize.toast("Address copied", 1000);
}

function clickDonateBtn() {
    $("#donateModal").modal('open');
}