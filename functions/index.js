// dev back end requires
const express = require('express');
const bodyParser = require('body-parser');
const crypto = require('crypto'),
    algorithm = 'aes-256-ctr';
const request = require('request');
const async = require('async');
const binance = require('node-binance-api');
const betterBinance = require('binance');
const bittrex = require('node-bittrex-api');
const Gdax = require('gdax');

// To handle CORS issue with angular client
// var cors = require('cors');
//

// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database. 
const admin = require('firebase-admin');
const app = express();
admin.initializeApp(functions.config().firebase);

// trying to use express
const db = admin.database();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
//

// Usefull methods
// Adding a unique function to Array prototype
Array.prototype.unique = function() {
    return this.filter(function (value, index, self) {
        return self.indexOf(value) === index;
    });
};

function getBittrexHoldings(node, callback) {

    const options = {
        url: 'https://api.fixer.io/latest?symbols=USD,EUR'
    };

    request(options, function (error, response, body) {
        if (error) {
            callback(new Error(error), null)
        }

        // Divide USD(T) by eurUsdRate to get some EUR
        const eurUsdRate = JSON.parse(body).rates.USD;

        bittrex.options({
            'apikey' : decrypt(node.api_key),
            'apisecret' : decrypt(node.secret)
        });

        bittrex.getbalances(function(data, err) {

            if (!err) {

                const holdings = {};
                holdings.total_in_euros = 0;
                // Filter all accounts and getting only >0 balances
                // data.result.filter(currency => currency.Balance !== 0).forEach(function(account) {
                //     bittrex.getmarkethistory({ market : account.Currency + '-BTC' }, function( data, err ) {
                //         console.log( data );
                //         holdings = holdings + (data.result[0].Price * account.Balance);
                //     });
                // });

                // MOC part
                // data.result[1].Balance = 10; // 10 BTC
                // data.result[8].Balance = 5; // 10 PAY
                // data.result[10].Balance = 15; // 10 USDT

                // Filter all accounts and getting only >0 balances
                async.each(data.result.filter(currency => currency.Balance !== 0), function(account, done) {

                    if (account.Currency === 'USDT') {

                        const usdtBalance = account.Balance;
                        const euroBalance = usdtBalance / eurUsdRate;

                        holdings.total_in_euros = holdings.total_in_euros + euroBalance;

                        holdings.USDT = {
                            balance: usdtBalance,
                            balance_in_euros: usdtBalance / eurUsdRate
                        };

                        done();
                    } else if (account.Currency === 'BTC') {
                        bittrex.getmarkethistory({ market : 'USDT-BTC' }, function(marketData, err ) {
                            if (!err) {

                                const btcBalance = account.Balance;
                                const euroBalance = (marketData.result[0].Price * btcBalance) / eurUsdRate;

                                holdings.total_in_euros = holdings.total_in_euros + euroBalance;

                                holdings.BTC = {
                                    balance: btcBalance,
                                    balance_in_euros: euroBalance
                                };

                            } else {
                                callback(new Error(err.message), null);
                            }
                            done();
                        });
                    } else {
                        bittrex.getmarkethistory({ market : 'BTC-' + account.Currency }, function(marketData, err ) {
                            if (!err) {

                                const btcBalance = marketData.result[0].Price * account.Balance;

                                bittrex.getmarkethistory({market: 'USDT-BTC'}, function(btcMarketdata, errBtcMarket) {
                                    const usdtBalance = btcMarketdata.result[0].Price * btcBalance;
                                    const euroBalance = usdtBalance / eurUsdRate;

                                    holdings.total_in_euros = holdings.total_in_euros + euroBalance;

                                    holdings[account.Currency] = {
                                        balance: account.Balance,
                                        balance_in_euros: euroBalance
                                    };

                                    done();
                                });

                            } else {
                                callback(new Error(err.message), null);
                            }
                        });
                    }


                }, function(err) {
                    if (!err) {
                        callback(null, holdings);
                        // console.log('holdings = ' + holdings);
                    } else {
                        callback(new Error(err.message), null);
                    }
                });
            } else {
                callback(new Error(err.message), null);
            }
        });

    });


}

function getBinanceHoldings(node, callback) {
    binance.options({
        'APIKEY': decrypt(node.api_key),
        'APISECRET': decrypt(node.secret)
    });

    try {
        binance.balance(function (balances) {

            binance.prices(function (ticker) {

                const holdings = {};
                holdings.total_in_euros = 0;

                const options = {
                    url: 'https://api.fixer.io/latest?symbols=USD,EUR'
                };

                request(options, function (error, response, body) {
                    if (error) {
                        callback(new Error(error), null)
                    }

                    // Divide USD(T) by eurUsdRate to get some EUR
                    const eurUsdRate = JSON.parse(body).rates.USD;

                    for (const account in balances) {
                        if (parseFloat(balances[account].available) !== parseFloat(0) || parseFloat(balances[account].onOrder) !== parseFloat(0)) {
                            if (account === "BTC") {

                                const btcBalance = parseFloat(balances[account].available) + parseFloat(balances[account].onOrder);
                                const euroBalance = (btcBalance * ticker["BTCUSDT"]) / eurUsdRate;

                                holdings.total_in_euros = holdings.total_in_euros + euroBalance;

                                // Summing the available and in order tokens
                                holdings.BTC = {
                                    balance: btcBalance,
                                    balance_in_euros: euroBalance
                                };

                            } else if (account === "USDT") {

                                const usdtBalance = parseFloat(balances[account].available) + parseFloat(balances[account].onOrder);
                                const euroBalance = usdtBalance / eurUsdRate;

                                holdings.total_in_euros = holdings.total_in_euros + euroBalance;

                                holdings.USDT = {
                                    balance: usdtBalance,
                                    balance_in_euros: euroBalance
                                }

                            } else {

                                if (ticker[account + "BTC"] !== undefined) {

                                    const btcBalance = (balances[account].available * ticker[account + "BTC"]) + (balances[account].onOrder * ticker[account + "BTC"]);
                                    const euroBalance = (btcBalance * ticker["BTCUSDT"]) / eurUsdRate;

                                    holdings.total_in_euros = holdings.total_in_euros + euroBalance;

                                    holdings[account] = {
                                        balance: btcBalance,
                                        balance_in_euros: euroBalance
                                    }
                                }
                            }
                        }
                    }
                    callback(null, holdings);
                });
            });
        })
    }
    catch (err) {
        callback(new Error(err), null);
    }
}

function getGdaxHoldings(node, callback) {
    try {
        // var crypto = require('crypto');

        const secret = decrypt(node.secret);
        const apiKey = decrypt(node.api_key);
        const passPhrase = decrypt(node.pass_phrase);

        const timeStamp = Date.now() / 1000;
        const requestPath = '/accounts';
        const method = 'GET';

        // create the prehash string by concatenating required parts
        const what = timeStamp + method + requestPath;

        // decode the base64 secret
        const key = Buffer(secret, 'base64');

        // create a sha256 hmac with the secret
        const hmac = crypto.createHmac('sha256', key);

        // sign the require message with the hmac
        // and finally base64 encode the result
        const signedMessage = hmac.update(what).digest('base64');

        const options = {
            url: 'https://api.gdax.com/accounts',
            headers: {
                'User-Agent': 'request',
                'CB-ACCESS-KEY': apiKey,
                'CB-ACCESS-SIGN': signedMessage,
                'CB-ACCESS-TIMESTAMP': timeStamp,
                'CB-ACCESS-PASSPHRASE': passPhrase
            }
        };

        request(options, function(error, response, body) {
            if (body === undefined || response.statusCode !== 200) {callback(new Error(response.statusMessage, null))}
            if (!error && response.statusCode === 200) {

                // Currencies except EUR
                const gdaxWalletsCurrency = JSON.parse(body).filter(function(account) {return account.currency !== 'EUR'}).map(function(account){return account.currency});

                const holdings = {};
                holdings.total_in_euros = parseFloat(JSON.parse(body).filter(function(account) {return account.currency === 'EUR'})[0].balance);
                holdings.EUR = {
                    balance: parseFloat(holdings.total_in_euros)
                };

                const personalInfo = JSON.parse(body);

                async.each(gdaxWalletsCurrency, function(currency, done) {
                    const currentCurrencyAccount = personalInfo.filter(function(account) {return account.currency === currency});
                    const currentCurrencyBalance = currentCurrencyAccount[0].balance;

                    const infoOptions = {
                        url: 'https://api.gdax.com/products/' + currency + '-EUR/ticker',
                        headers: {
                            'User-Agent': 'request',
                            'CB-ACCESS-KEY': apiKey,
                            'CB-ACCESS-SIGN': signedMessage,
                            'CB-ACCESS-TIMESTAMP': timeStamp,
                            'CB-ACCESS-PASSPHRASE': passPhrase
                        }
                    };

                    request(infoOptions, function (errorInfo, responseInfo, bodyInfo) {
                        if (body === undefined || response.statusCode !== 200) {callback(new Error(response.statusMessage, null))}
                        if (!error && response.statusCode === 200) {
                            const marketInfo = JSON.parse(bodyInfo);
                            const currencyActualPrice = marketInfo.price;

                            // var tokenData = {
                            //     [currency]: {
                            //         balance: currentCurrencyBalance,
                            //         balance_in_euros: currencyActualPrice * currentCurrencyBalance
                            //     }
                            // };

                            if (parseFloat(currentCurrencyBalance) !== 0) {
                                holdings[currency] = {
                                    balance: parseFloat(currentCurrencyBalance),
                                    balance_in_euros: currencyActualPrice * currentCurrencyBalance
                                };

                                holdings.total_in_euros = parseFloat(holdings.total_in_euros) + (currencyActualPrice * currentCurrencyBalance);
                            }

                            done();
                        }
                    });
                }, function(err) {
                    callback(null, holdings);
                });
            }
        });
    } catch(error) {
        callback(new Error(error), null);
    }
}

function aggregateHoldings(uid, callback) {
    db.ref(uid + '/config/exchanges/').once("value", function(data) {

        const holdings = {};

        if (data.val() !== null) {
            // Getting every exchanges which are enabled in DB
            async.each(Object.keys(data.val()).filter(exchange => data.val()[exchange].enabled), function(exchange, done) {

                const exchangeNode = data.val()[exchange];

                switch (exchange) {
                    case 'gdax':
                        getGdaxHoldings(exchangeNode, function(gdaxError, gdaxHoldings) {
                            holdings.gdax = gdaxHoldings;
                            done();
                        });
                        break;
                    case 'binance':
                        getBinanceHoldings(exchangeNode, function(binanceError, binanceHoldings) {
                            holdings.binance = binanceHoldings;
                            done();
                        });
                        break;
                    case 'bittrex':
                        getBittrexHoldings(exchangeNode, function(bittrexError, bittrexHoldings) {
                            holdings.bittrex = bittrexHoldings;
                            done();
                        });
                        break;
                    default:
                        break;
                }
            }, function(err) {
                callback(null, holdings);
            });
        } else {
            callback(new Error('No API keys found'), null);
        }

    })
        .catch(function(error) {
            callback(new Error(error), null);
        })
}

function checkApiKeysValidity(exchangeName, apikeys, callback) {
    switch (exchangeName) {
        case 'gdax':

            const secret = apikeys.secret;
            const apiKey = apikeys.api_key;
            const passPhrase = apikeys.pass_phrase;

            const timeStamp = Date.now() / 1000;
            const requestPath = '/accounts';
            const method = 'GET';

            // create the prehash string by concatenating required parts
            const what = timeStamp + method + requestPath;

            // decode the base64 secret
            const key = Buffer(secret, 'base64');

            // create a sha256 hmac with the secret
            const hmac = crypto.createHmac('sha256', key);

            // sign the require message with the hmac
            // and finally base64 encode the result
            const signedMessage = hmac.update(what).digest('base64');

            const options = {
                url: 'https://api.gdax.com/accounts',
                headers: {
                    'User-Agent': 'request',
                    'CB-ACCESS-KEY': apiKey,
                    'CB-ACCESS-SIGN': signedMessage,
                    'CB-ACCESS-TIMESTAMP': timeStamp,
                    'CB-ACCESS-PASSPHRASE': passPhrase
                }
            };

            request(options, function(error, response, body) {
                if (error || JSON.parse(body).message !== undefined) {
                    if (JSON.parse(body).message === 'Invalid API Key') {
                        callback(new Error('Invalid API Keys'), null);
                    } else {
                        callback(new Error(JSON.parse(body).message), null);
                    }
                } else {

                    const encryptedKeys = {};
                    encryptedKeys.apiKey = encrypt(apiKey);
                    encryptedKeys.secret = encrypt(secret);
                    encryptedKeys.passPhrase = encrypt(passPhrase);
                    callback(null, encryptedKeys);
                }
            });

            break;
        case 'binance':
            const binanceRest = new betterBinance.BinanceRest({
                key: apikeys.api_key, // Get this from your account on binance.com
                secret: apikeys.secret, // Same for this
                timeout: 15000, // Optional, defaults to 15000, is the request time out in milliseconds
                recvWindow: 100000, // Optional, defaults to 5000, increase if you're getting timestamp errors
                disableBeautification: false
                /*
                 * Optional, default is false. Binance's API returns objects with lots of one letter keys.  By
                 * default those keys will be replaced with more descriptive, longer ones.
                 */
            });

            binanceRest.account()
                .then(function(data) {
                    const encryptedKeys = {};
                    encryptedKeys.apiKey = encrypt(apikeys.api_key);
                    encryptedKeys.secret = encrypt(apikeys.secret);
                    callback(null, encryptedKeys);
                })
                .catch(function(error) {
                    callback(new Error(error.msg), null);
                });
            break;

        case 'bittrex':
            bittrex.options({
                'apikey' : apikeys.api_key,
                'apisecret' : apikeys.secret
            });

            bittrex.getbalances(function(data, err) {

                if (!err) {
                    const encryptedKeys = {};
                    encryptedKeys.apiKey = encrypt(apikeys.api_key);
                    encryptedKeys.secret = encrypt(apikeys.secret);
                    callback(null, encryptedKeys);
                } else {
                    callback(new Error(err.message), null);
                }
            });
            break;
    }
}

function addFormatedBinanceOrders(orders, data) {
    return new Promise((resolve, reject) => {

        try {
            if (data.length > 0) {
                const symbol = data[0].symbol;
                orders[symbol] = {};
                orders[symbol].orders = [];
                const filledOrders = data.filter(order => order.status === 'FILLED');

                async.each(filledOrders, (order, done) => {
                    // Deleting every attributes useless from binance
                    delete order.clientOrderId;
                    delete order.icebergQty;
                    delete order.isWorking;
                    delete order.orderId;
                    delete order.origQty;
                    delete order.status;
                    delete order.stopPrice;
                    // Keeping symbol -- necessary ??
                    delete order.symbol;
                    delete order.timeInForce;
                    delete order.type;

                    // creating my personnal order object
                    order.quantity = order.executedQty;
                    delete order.executedQty;

                    // Adding proper orders to orders outside object
                    orders[symbol].orders.push(order);
                    done();
                }, (error) => {
                    if (!error) {
                        resolve();
                    } else {
                        reject(error);
                    }
                });
            } else {
                resolve();
            }

        } catch (error) {
            reject(error);
        }
    })
}

function getBinanceOrders(node, currency) {
    return new Promise(function(resolve, reject) {
        const binanceRest = new betterBinance.BinanceRest({
            key: decrypt(node.api_key), // Get this from your account on binance.com
            secret: decrypt(node.secret), // Same for this
            timeout: 15000, // Optional, defaults to 15000, is the request time out in milliseconds
            recvWindow: 100000, // Optional, defaults to 5000, increase if you're getting timestamp errors
            disableBeautification: false
            /*
             * Optional, default is false. Binance's API returns objects with lots of one letter keys.  By
             * default those keys will be replaced with more descriptive, longer ones.
             */
        });

        binanceRest.exchangeInfo()
            .then(exchangeInfos => {
                const currencyFormated = currency.toString().toLocaleUpperCase();
                let orders = {};
                const marketsAvailableForThisCurreyncy = exchangeInfos.symbols.filter(symbol => symbol.baseAsset === currencyFormated);

                async.each(marketsAvailableForThisCurreyncy, (market, done) => {
                    binanceRest.allOrders({
                        symbol: currencyFormated + market.quoteAsset  // Object is transformed into a query string, timestamp is automatically added
                    })
                        .then(data => {
                            addFormatedBinanceOrders(orders, data)
                                .then(() => {
                                    // If there are some orders for this symbol
                                    const currentMarket = orders[market.symbol];
                                    if (currentMarket !== undefined) {
                                        currentMarket.currency = market.baseAsset;
                                        currentMarket.currencyMarket = market.quoteAsset;
                                    }
                                    done();
                                })
                                .catch(error => {
                                    done();
                                    reject(error);
                                })
                        })
                        .catch(error => {
                            done();
                            reject(error);
                        })
                }, (error) => {
                    if (!error) {
                        resolve(orders);
                    } else {
                        reject(error);
                    }
                });
            })
            .catch(error => {
                reject(error);
            });
    })
}

function getGdaxOrders(node, currency) {
    return new Promise(function(resolve, reject) {
        const key = decrypt(node.api_key);
        const secret = decrypt(node.secret);
        const passphrase = decrypt(node.pass_phrase);

        const apiURI = 'https://api.gdax.com';
        // const sandboxURI = 'https://api-public.sandbox.gdax.com';

        const authedClient = new Gdax.AuthenticatedClient(
            key,
            secret,
            passphrase,
            apiURI
        );

        authedClient.getFills()
            .then((fills) => {

                let orders = {};

                // Orders of that very currency
                const currencyOrders = fills.filter(order => order.product_id.startsWith(currency.toString().toUpperCase()));

                // Markets used for this currency
                const marketsUsed = currencyOrders.map(order => order.product_id).unique();

                marketsUsed.forEach(market => {

                    // getting orders for this particular market
                    const marketOrders = currencyOrders.filter(order => order.product_id === market);

                    orders[market] = {};
                    orders[market].currency = market.split("-")[0];
                    orders[market].currencyMarket = market.split("-")[1];

                    orders[market].orders = marketOrders.map(order => {
                        delete order.fee;
                        delete order.liquidity;
                        delete order.order_id;
                        delete order.product_id;
                        delete order.settled;
                        delete order.trade_id;
                        delete order.usd_volume;
                        delete order.user_id;
                        delete order.profile_id;

                        order.time = Math.round(new Date(order.created_at).getTime());
                        delete order.created_at;

                        order.quantity = order.size;
                        delete order.size;

                        order.side = order.side.toString().toUpperCase();

                        return order;
                    })
                });

                resolve(orders);
            })
            .catch((error) => {
                console.log(error);
                resolve(error);
            })
    })
}

function getAllOrders(uid, exchange, currency) {

    return new Promise(function(resolve, reject) {

        db.ref(uid + '/config/exchanges/').once('value', function(data) {
            const exchangeNode = data.val()[exchange];

            switch(exchange) {
                case 'binance':
                    getBinanceOrders(exchangeNode, currency)
                        .then(function(orders) {
                            resolve(orders);
                        })
                        .catch(function(error) {
                            if (error.msg !== undefined) {
                                error.message = error.msg;
                            }
                            reject(error);
                        });
                    break;
                case 'gdax':
                    getGdaxOrders(exchangeNode, currency)
                        .then((orders) => {
                            resolve(orders);
                        })
                        .catch((error) => {
                            reject(error);
                        });
                    // reject(new Error('Not available yet'));
                    break;
                case 'bittrex':
                    reject(new Error('Not available yet'));
                    break;
            }
        });
    })
}

// Encrypt api keys part
function encrypt(text){
    const cipher = crypto.createCipher(algorithm, functions.config().encryption.password);
    let crypted = cipher.update(text,'utf8','hex');
    crypted += cipher.final('hex');
    return crypted;
}

function decrypt(text){
    const decipher = crypto.createDecipher(algorithm, functions.config().encryption.password);
    let dec = decipher.update(text,'hex','utf8');
    dec += decipher.final('utf8');
    return dec;
}

// function getHoldings(req, res) {
//     // So dirty - but only for dev/tests purposes --> TODO: Change that !
//     if (req.query.dailyStoreTaskDev === "true" && req.query.uid !== null) {
//         aggregateHoldings(req.query.uid, function(err, holdings) {
//             if (err) {
//                 res.status(500).send(err.message);
//             } else {
//                 res.send(holdings);
//             }
//         })
//     } else {
//         // Getting userId from client user token
//         admin.auth().verifyIdToken(req.headers['token'])
//             .then(function(decodedToken) {
//
//                 const uid = decodedToken.uid;
//
//                 aggregateHoldings(uid, function(err, holdings) {
//                     if (err) {
//                         res.status(500).send(err.message);
//                     } else {
//                         res.send(holdings);
//                     }
//                 })
//
//             })
//             .catch(function(error) {
//                 res.status(500).send(error.message);
//             });
//     }
// }
//

// Public endpoints
exports.dailyStoreTaskDev = functions.https.onRequest((req, res) => {

    new Promise(function(resolve, reject) {
        function listAllUsers(nextPageToken) {

            // Getting all users
            admin.auth().listUsers(1000, nextPageToken)
                .then(function(listUsersResult) {

                    // Calculating holdings for all users (asynchronously)
                    async.each(listUsersResult.users, function(user, done) {

                        // Calculating holdings for current user
                        aggregateHoldings(user.uid, function(error, data) {
                            if (error) {

                                done();
                            } else {
                                data.date = Date.now();
                                // Push the new message into the Realtime Database using the Firebase Admin SDK.
                                admin.database().ref(user.uid + '/historic/').push(data).then(snapshot => {

                                    done();
                                }).catch(error => {

                                    done();
                                })


                            }
                        })
                    }, function(error) {
                        if (listUsersResult.pageToken) {
                            listAllUsers(listUsersResult.pageToken);
                        } else {
                            resolve();
                        }
                    })
                })
                .catch(function(error) {
                    reject(error);
                });
        }
        listAllUsers();
    })
        .then(function(test) {
            console.log(test);
            res.send('ok');
        })
        .catch(function(error) {
            res.status(500).send(error);
        })

});

exports.holdings = functions.https.onRequest((req, res) => {
    // Getting userId from client user token
    admin.auth().verifyIdToken(req.headers['token'])
        .then(function(decodedToken) {

            const uid = decodedToken.uid;

            aggregateHoldings(uid, function(err, holdings) {
                if (err) {
                    res.status(500).send(err.message);
                } else {
                    res.send(holdings);
                }
            })

        })
        .catch(function(error) {
            res.status(500).send(error.message);
        });
});

exports.orders = functions.https.onRequest((req, res) => {
    admin.auth().verifyIdToken(req.headers['token'])
        .then(function(decodedToken) {
            const uid = decodedToken.uid;
            getAllOrders(uid, req.query.exchange, req.query.currency)
                .then(function(orders) {
                    res.send(orders);
                })
                .catch(function(error) {
                    res.status(500).send(error.message);
                })
        })
        .catch(function(error) {
            res.status(500).send(error.message);
        })
});

exports.validateApiKeys = functions.https.onRequest((req, res) => {

    checkApiKeysValidity(req.query.exchange, req.query, function(error, encryptedKeys) {
    if (!error) {
        // encryptedApiKeys.
        res.send(encryptedKeys);
    } else {
        res.status(500).send(error.message);
    }});

    // cors((req, res, () => {
    //
    //         checkApiKeysValidity(req.query.exchange, req.query, function(error, areApiKeysValid) {
    //         if (!error) {
    //             res.send(true);
    //         } else {
    //             res.status(500).send(error.message);
    //         }
    //     })
    //
    // }))

});

// exports.exchanges = functions.https.onRequest((req, res) => {
//     res.redirect('/');
// });

// exports.linkUser = functions.https.onRequest((req, res) => {
//
// });