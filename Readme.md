# Cryptopus

## Project description
This project shows an overview of users crypto savings on several exchanges.
It also shows some historic data about their savings, and some other things.

**For now it does not work properly** 

A better description of the project can be found on [Steemit](https://steemit.com/cryptopus/@ghyslainbruno/cryptopus).

## When Cryptopus came in my developer journey - Dec 2017. 
I worked on Cryptopus at the very beginning of my **web** developer life. I developed it without using any framework (React, Angular...). That's why it's a bit messy and a hard to understand architecture. But anyway, it was a very trainer project. 

## What I learned with Cryptopus
With Cryptopus I started to fall in love with web development, and I understood that a solid stack **HAS** to be used. 

That's what I'm doing now with all my other projects.

### Cryptopus : https://cryptopus-ad335.firebaseapp.com (the project)

# Personal cheat sheet
## Cloud Functions root URL
https://us-central1-cryptopus.cloudfunctions.net/


## Endpoints : 

Every endpoint is available in a Firebase CloudFunction. 

In order to test these endpoints, a "funtions_test" is also available inside the project, 
which is basically the very same endpoints served by an NodeJs express server. 

Here are the different available endpoints : 

* **/holdings** : 1 GET request types
   
   * Using custom headers : { token : 'tokenID' }
   * Response example : 
   ```json
   {  
      "fooFirebaseUserId":{  
         "historic":{  
            "-L0niWAD0z2VEBTsVvpJ":{  
               "ecxhange":{  
                  "token":{  
                     "balance":10,
                     "balance_in_euros":100
                  },
                  "total_in_euros":100
               }
            }
         }
      }
   }
   ```
        
* **/dailyStoreTaskDev** : 1 GET request type

Will get every holdings for every account in DB (if exchanges API keys are stored) and put them in the firebase DB "/historic" node, with the time of the moment at the "date" node.
Responds "ok" if all is ok, 500 error if not. 
       
        
* **/validateApiKeys** :  1 GET request type
Checks if the API keys passed are valid. 

    Parameters : 
    ```json
    {  
       "exchange": "exchangeName",
       "api_key": "personnalApiKey",
       "pass_phrase": "personnalPassPhrase",
       "secret": "personnalSecret"
    }
    ```
        
*To test Daily holdings report in database, https://cron-job.org can be used with these credentials :* 
* *user: ghyslainbruno@gmail.com*
* *password: PXg3nYzUgjPjpCjK*

*Cron jobs can be done using this service - an example request is already updated up there.*

## Build : 

For now, no automatic build is done, starting to use uglifyjs ([uglify-es npm package](https://www.npmjs.com/package/uglify-es))
to minify/uglify every JS files in one "index.js" file in the app/ directory (do not mix up with "index.js" file in the functions/ directory, which is basically the back-end side).

(In the future should rename index.js file with a version number, or at least, put it as a request URl in the script html balise inside index.html)

Command to uglify JS files : 

``jsFiles=`find app/js -type f | grep '.*\.js$'` && uglifyjs $jsFiles > app/index.min.js``


To run for develop mode, type : ``./builder.sh dev``

> Simply uncomment js files script tags in index.html and comment script tag for index.min.js

To run for production mode, type : ``./builder.sh prod 0.0.1``

> Comment all js files script tags in index.html, uncomment index.min.js script tag, minify, concatenate every js file in index.min.js and add ?v0.0.1 at the index.min.js in order to force cache reload. 
> Change 0.0.1 by the version number wanted

*Every command using builder.sh creates a "before.html" file which is the copy of the index.html file before the command was used.*